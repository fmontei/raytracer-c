#include <stdio.h>
#include <stdbool.h>
#include <math.h>
//#include "vector.h"

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b; 
}  color_c; 

typedef struct {
  double r;
  double g;
  double b; 
}  color_d;

typedef struct {
  double x; 
  double y; 
  double z; 
}  vector_t; 

typedef struct {
  double x; 
  double y; 
  double z; 
}  point_t; 

   const int height = 500; 
   const int width  = 500; 
   const int maxV   = 255; 
   const int radius = 3; 
 
   // Vector N
   vector_t VecN;

   // Vector L
   vector_t VecL; 

   
     
   double square (double);
   void normalize_ray_direction (vector_t *); 
   bool intersect_sphere (vector_t);
   void find_intersection (double);  
   void print_ppm_header (FILE *out);   
   color_c do_lighting (color_d obj_color);  
   double dot (vector_t *v1, vector_t *v2);
   void translate_space_dimension (vector_t *v, double *, double *);


int main (void)
{ 
    double x, y;  

    // Used to hold values of destination of vector
    vector_t D; 

    // Declare and initialize initial obj_color;
    color_d obj_color; 
    obj_color.r = 1.0;
    obj_color.g = 0.0;
    obj_color.b = 0.0;

    color_c outcol; 
    bool intersect; 
      
    FILE *out;
    out = fopen ("output.ppm", "w"); 
    
    print_ppm_header (out); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < height; ++y ) 
    {
         for ( x = 0; x < width; ++x ) 
         {  
              // Convert 2D space to 3D space
              translate_space_dimension (&D, &x, &y);
               
              // Normalize 
              normalize_ray_direction (&D);
              
              intersect = intersect_sphere (D); 
              
              //fprintf (stderr, "%.3f\n", t); 
              if (intersect)
              {
                  outcol = do_lighting (obj_color); 
                  fprintf (out, "%c%c%c", outcol.r, outcol.g, outcol.b);  
                  //fprintf (stderr, "%c\n", outcol.r);
              }
              else
                  fprintf (out, "%c%c%c", 0, 0, 0); 
         }
    } 
    
    return 0; 
}

void translate_space_dimension (vector_t *v, double *x, double *y)
{
    v->x =  (*x / width) - 0.5; 
    v->y =  0.5 - (*y / height); 
    v->z =  1.0;
}   

bool intersect_sphere (vector_t D)
{
    double A, B, C, dis, t0, t1, t; 
    
    // Intersect points
    point_t I;

    // Normalized intersect points
    point_t N; 
    
    // Eye point (origin)
    point_t O; 
    O.x, O.y, O.z = 0.0;  

    // Center point of circle
    point_t Cir;
    Cir.x =  0.0; 
    Cir.y =  0.0; 
    Cir.z = 10.0;
  
    // Light source location
    point_t Lit; 
    Lit.x = -10.0; 
    Lit.y =  10.0; 
    Lit.z =   5.0;  

    A = 1; 
    B = 2 * ( D.x * (O.x - Cir.x) + D.y * (O.y - Cir.y) + D.z * (O.z - Cir.z) ); 
    C = ( pow(O.x - Cir.x, 2) + pow(O.y - Cir.y, 2) + pow(O.z - Cir.z, 2) )
          - pow(radius, 2);  

    // Discriminant is what's inside the square root
    dis = B * B - 4.0 * A * C; 

    //printf ("%.3f %.3f %.3f\n", Xd, Yd, Zd); 
    //fprintf (stderr, "%.3f %.3f %.3f dis = %.3f\n", A, B, C, dis);

    if (dis >= 0.0 && A != 0.0)
    {
        t0  = ( -B - sqrt (dis) ) / 2.0 * A; 
        t1  = ( -B + sqrt (dis) ) / 2.0 * A;
        //fprintf (stderr, "%.3f %.3f\n", t0, t1); 
        if (t0 < t1 && t0 > 0.0)
        {
            t = t0;
            //fprintf (stderr, "%.3f\n", *t); 
        }

        else if (t1 > t0 && t1 > 0.0)
            t = t1;
        
        // Calculate intersect points
        I.x = (O.x + D.x) * t; 
        I.y = (O.y + D.y) * t; 
        I.z = (O.z + D.z) * t; 
  
        // Normalize intersect points
        N.x = (I.x - Cir.x) / radius;
        N.y = (I.y - Cir.y) / radius;
        N.z = (I.z - Cir.z) / radius; 

        // Calculate Vector N
        VecN.x = (I.x - Cir.x) / radius;
        VecN.y = (I.y - Cir.y) / radius;
        VecN.z = (I.z - Cir.z) / radius;

        // Calculate Vector L
        VecL.x = (Lit.x - I.x) / radius; 
        VecL.y = (Lit.y - I.y) / radius; 
        VecL.z = (Lit.z - I.z) / radius;
        
        return true;
    }

    else
        return false;   
} 

color_c do_lighting (color_d obj_color)
{
    color_d fcol; 
    color_c outcol;    
    
    fcol.r = 0.10 * obj_color.r; 
    fcol.g = 0.10 * obj_color.g;
    fcol.b = 0.10 * obj_color.b;

    fcol.r += dot(&VecN, &VecL) * obj_color.r; 
    fcol.g += dot(&VecN, &VecL) * obj_color.g; 
    fcol.b += dot(&VecN, &VecL) * obj_color.b; 
 
    //fprintf (stderr, "%.3f\n", result); 

    outcol.r = (unsigned char) (fcol.r * 255);
    outcol.g = (unsigned char) (fcol.g * 255);
    outcol.b = (unsigned char) (fcol.b * 255);

    //fprintf (stderr, "%hhu\n", outcol.b); 

    return outcol; 
    
}

double dot (vector_t *v1, vector_t *v2)
{
    double result;
    result = (v1->x * v2->x) + (v1->y * v2->y) + (v1->z * v2->z); 
    //fprintf (stderr, "%.3f\n", result); 
    return result; 
     
}        

void print_ppm_header (FILE *out)
{
    fprintf (out, "P6\n%d %d\n%d\n", width, height, maxV); 
}

void normalize_ray_direction (vector_t *v)
{
    double length; 
    
    length = sqrt ( pow(v->x, 2) + pow(v->y, 2) + pow(v->z, 2) ); 
     
    v->x /= length;
    v->y /= length;
    v->z /= length; 
}



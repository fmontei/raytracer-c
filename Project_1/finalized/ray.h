#include "vector.h"

#define HEIGHT 500
#define WIDTH  500
#define MAXV   255 

// RGB values ending in d = type double
typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
  double rd;
  double gd;
  double bd;  
}  PIXEL_T; 

typedef struct {
  VEC_PT or;
  VEC_PT dir;
}  RAY_T;

// Sphere structure
typedef struct {
  VEC_PT center;
  double radius;  
}  SPHERE_T; 

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "ray.h"

///////////////////////////////////////////
// Felipe Monteiro                       //
// cpsc102                               //
// Dr. Davis                             // 
// May 22, 2013                          //
// Project 1 -- Basic Ray Tracer         //
//                                       //
// Program that uses a basic ray tracer  //
// to create a red sphere with ambient,  // 
// diffuse and specular lighting.        //
///////////////////////////////////////////


/* RAY-SPECIFIC FUNCTION PROTOTYPES */  
void print_ppm_header (FILE *);   
bool intersect_sphere (SPHERE_T, RAY_T, VEC_PT *, VEC_PT *);    
PIXEL_T do_lighting (RAY_T, VEC_PT, VEC_PT);

int main (void)
{  
    int x, y;  
    bool intersect; 

    // final_color holds floating-point RGB values
    // write_color holds unsigned char RGB values    
    PIXEL_T background, final_color, write_color;
    background.r = 0;
    background.g = 0;
    background.b = 0; 

    // Define the ray's initial characteristics
    RAY_T ray;
    ray.or.x = 0;
    ray.or.y = 0;
    ray.or.z = 0;
    
    // Vectors that will be sent to two functions: intersect_sphere
    // and do_lighting
    VEC_PT norm, intersect_pt; 
     
    // Define the sphere's initial characteristics
    SPHERE_T sphere;
    sphere.radius   =  3; 
    sphere.center.x =  0; 
    sphere.center.y =  0; 
    sphere.center.z = 10;   
      
    FILE *out;
    out = fopen ("img.ppm", "w");  

    print_ppm_header (out); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < HEIGHT; ++y ) 
    {
         for ( x = 0; x < WIDTH; ++x ) 
         {  
              // Convert ray.dir from 2D space to 3D space
              ray.dir.x = -0.5 + ( (double) x / WIDTH  ); 
              ray.dir.y =  0.5 - ( (double) y / HEIGHT ); 
              ray.dir.z =  1.0;
               
              // Normalize ray direction
              ray.dir = vec_normalize (ray.dir);
              
              // Calculate where the direction ray intersects the sphere
              intersect = intersect_sphere (sphere, ray, &intersect_pt, &norm); 
             
              // If the ray intersects, print a pixel at that location
              if (intersect)
              {
                  // Calculate ambient, diffuse, and spectral lighting, 
                  // all three of which are combined within final_color
                  final_color = do_lighting (ray, intersect_pt, norm);
                  
                  if (final_color.rd > 1)
                      final_color.rd = 1; 

                  if (final_color.gd > 1)
                      final_color.gd = 1; 

                  if (final_color.bd > 1)
                      final_color.bd = 1; 

                  // Convert final_color from type double to type unsigned char
                  write_color.r = (unsigned char) (final_color.rd * 255);
                  write_color.g = (unsigned char) (final_color.gd * 255);
                  write_color.b = (unsigned char) (final_color.bd * 255);
                 
                  fprintf (out, "%c%c%c", write_color.r, write_color.g, write_color.b); 
              }
              
              // Else if ray does not intersect sphere, print background color
              else
                  fprintf (out, "%c%c%c", background.r, background.g, background.b); 
         }
    } 
    
    fclose (out); 

    return 0; 
}

void print_ppm_header (FILE *out)
{
    fprintf (out, "P6\n%d %d\n%d\n", WIDTH, HEIGHT, MAXV); 
}

bool intersect_sphere (SPHERE_T sphere, RAY_T ray, VEC_PT *int_pt, VEC_PT *VecN)
{
    double A, B, C, discriminant, t0, t1, t;   
      
    A = 1;  // A is effectively a tentative variable for now
    B = 2 * ( ray.dir.x * (ray.or.x - sphere.center.x) + 
              ray.dir.y * (ray.or.y - sphere.center.y) + 
              ray.dir.z * (ray.or.z - sphere.center.z) ); 
    C =     ( (ray.or.x - sphere.center.x) * (ray.or.x - sphere.center.x) + 
              (ray.or.y - sphere.center.y) * (ray.or.y - sphere.center.y) +
              (ray.or.z - sphere.center.z) * (ray.or.z - sphere.center.z) -
              (sphere.radius) * (sphere.radius) );  

    // Calculate the discriminant, the part of the quadratic formula inside the square root
    discriminant = B * B - (4 * C); 

    if (discriminant < 0)
        return false; 
    
    t0  = ( -B - sqrt (discriminant) ) / 2;  // A is omitted since it is equal to 1 
    t1  = ( -B + sqrt (discriminant) ) / 2;
      
    // If the ray is moving in the negative direction, return false
    if (t0 < 0 && t1 < 0) 
        return false;  

    if (t0 < t1 && t0 > 0)
        t = t0;

    else if (t1 < t0 && t1 > 0)
        t = t1;

    // Calculate intersect points, points where ray and surface of sphere intersect
    int_pt->x = ray.or.x + ray.dir.x * t; 
    int_pt->y = ray.or.y + ray.dir.y * t;
    int_pt->z = ray.or.z + ray.dir.z * t;

    // Calculate Vector N (normal)
    VecN->x = (int_pt->x - sphere.center.x) / sphere.radius; 
    VecN->y = (int_pt->y - sphere.center.y) / sphere.radius; 
    VecN->z = (int_pt->z - sphere.center.z) / sphere.radius;  

    return true; 
} 

PIXEL_T do_lighting (RAY_T ray, VEC_PT int_pt, VEC_PT VecN)
{
    const int n = 100;                 // Light intensity
    const double amb_factor = 0.10;   // Ambient multiple factor
    VEC_PT VecL, VecR, light_src;      // VecL = Light vector; VecR = reflected ray 
    PIXEL_T sphere_color, final_color;

    sphere_color.rd = 1; 
    sphere_color.gd = 0;
    sphere_color.bd = 0;

    light_src.x = -10;
    light_src.y =  10;
    light_src.z =   5; 

    // Calculate ambient lighting
    final_color.rd = amb_factor * sphere_color.rd; 
    final_color.gd = amb_factor * sphere_color.gd;
    final_color.bd = amb_factor * sphere_color.bd;

    // Calculate Vector L (Light)
    VecL = vec_subtract (light_src, int_pt); 

    VecL = vec_normalize (VecL);

    // Cosine theta = vec_dot(N, L)
    // If cosine theta is greater than 0,
    // add diffuse lighting to ambient lighting
    if ( vec_dot(VecN, VecL) > 0 ) 
    {
        final_color.rd += vec_dot(VecN, VecL) * sphere_color.rd; 
        final_color.gd += vec_dot(VecN, VecL) * sphere_color.gd; 
        final_color.bd += vec_dot(VecN, VecL) * sphere_color.bd; 
    
        // Calculate Vector R
        VecR.x = VecL.x - ( 2 * vec_dot(VecN, VecL) * VecN.x ); 
        VecR.y = VecL.y - ( 2 * vec_dot(VecN, VecL) * VecN.y );
        VecR.z = VecL.z - ( 2 * vec_dot(VecN, VecL) * VecN.z );

        VecR = vec_normalize (VecR);
    
        // Add specular lighting
        final_color.rd += pow ( vec_dot(VecR, ray.dir), n ); 
        final_color.gd += pow ( vec_dot(VecR, ray.dir), n );  
        final_color.bd += pow ( vec_dot(VecR, ray.dir), n );  
    }

    return final_color;  
} 


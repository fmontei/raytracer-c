#include <stdio.h>
#include <stdbool.h>
#include <math.h>
//#include "vector.h"

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b; 
}  color_c; 

typedef struct {
  double r;
  double g;
  double b; 
}  color_d;

typedef struct {
  double x; 
  double y; 
  double z; 
}  vector_t; 

typedef struct {
  double x; 
  double y; 
  double z; 
}  point_t; 
   
     
    void normalize_vector (vector_t *); 
    bool intersect_sphere (vector_t *, vector_t *, vector_t *, vector_t *, point_t, 
                           point_t, point_t, point_t);
    void find_intersection (double);  
    void print_ppm_header (FILE *out);   
    color_c do_lighting (color_d, vector_t, vector_t, vector_t, vector_t);  
    double dot (vector_t *v1, vector_t *v2);
    void translate_space_dimension (vector_t *v, double *, double *);

    const int height = 500; 
    const int width  = 500; 
    const int maxV   = 255; 
    const int radius = 3;


int main (void)
{  
    double x, y;  

    // Vector D
    vector_t VecD; 
    
    // Vector N
    vector_t VecN;
    VecN.x = 0.0; 
    VecN.y = 0.0; 
    VecN.z = 0.0; 
    
    // Vector L
    vector_t VecL;
    VecL.x = 0.0; 
    VecL.y = 0.0; 
    VecL.z = 0.0;

    // Vector R
    vector_t VecR; 
    VecR.x = 0.0; 
    VecR.y = 0.0; 
    VecR.z = 0.0;

    // Sphere color is red 
    color_d obj_color; 
    obj_color.r = 1.0;
    obj_color.g = 0.0;
    obj_color.b = 0.0;

    // Eye point (origin)
    point_t O; 
    O.x, O.y, O.z = 0.0;  

    // Center point of circle
    point_t Cir;
    Cir.x =  0.0; 
    Cir.y =  0.0; 
    Cir.z = 10.0;
  
    // Light source location
    point_t Lit; 
    Lit.x = -10.0; 
    Lit.y =  10.0; 
    Lit.z =   5.0;

    // Intersect points
    point_t I;
    I.x = 0.0; 
    I.y = 0.0; 
    I.z = 0.0; 

    color_c outcol; 
    bool intersect; 
      
    FILE *out;
    //out = fopen ("output.ppm", "w"); 
    out = stdout; 
    print_ppm_header (out); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < height; ++y ) 
    {
         for ( x = 0; x < width; ++x ) 
         {  
              // Convert 2D space to 3D space
              translate_space_dimension (&VecD, &x, &y);
               
              // Normalize 
              normalize_vector (&VecD);
              
              intersect = intersect_sphere (&VecD, &VecN, &VecL, &VecR, O, Cir, Lit, I); 
              
              //fprintf (stderr, "%.3f\n", t); 
              if (intersect)
              {
                  outcol = do_lighting (obj_color, VecD, VecN, VecL, VecR); 
                  fprintf (out, "%c%c%c", outcol.r, outcol.g, outcol.b); 
                    
                  //fprintf (stderr, "%c\n", outcol.r);
              }
              else
                  fprintf (out, "%c%c%c", 0, 0, 0); 
         }
    } 
    
    fclose (out); 

    return 0; 
}

void translate_space_dimension (vector_t *v, double *x, double *y)
{
    v->x =  -0.5 + (*x / width); 
    v->y =   0.5 - (*y / height); 
    v->z =   1.0;
}   

bool intersect_sphere (vector_t *D, vector_t *VecN, vector_t *VecL, vector_t *VecR, 
                        point_t O, point_t Cir, point_t Lit, point_t I)
{
    double A, B, C, dis, t0, t1, t;  
      
    A = 1; 
    B = 2 * ( D->x * (O.x - Cir.x) + D->y * (O.y - Cir.y) + D->z * (O.z - Cir.z) ); 
    C = ( pow(O.x - Cir.x, 2) + pow(O.y - Cir.y, 2) + pow(O.z - Cir.z, 2) )
          - pow(radius, 2);  

    // Discriminant is what's inside the square root
    dis = B * B - (4.0 * A * C); 

    //printf ("%.3f %.3f %.3f\n", Xd, Yd, Zd); 
    //fprintf (stderr, "%.3f %.3f %.3f dis = %.3f\n", A, B, C, dis);

    if (dis >= 0.0 && A != 0.0)
    {
        t0  = ( -B - sqrt (dis) ) / 2.0 * A; 
        t1  = ( -B + sqrt (dis) ) / 2.0 * A;
        //fprintf (stderr, "%.3f %.3f\n", t0, t1); 
        if (t0 < t1 && t0 > 0.0)
        {
            t = t0;
            //fprintf (stderr, "%.3f\n", t); 
        }

        else if (t1 < t0 && t1 > 0.0)
            t = t1;
        
        // Calculate intersect points
        I.x = O.x + D->x * t; 
        I.y = O.y + D->y * t; 
        I.z = O.z + D->z * t; 

        // Calculate Vector N
        VecN->x = (I.x - Cir.x) / radius; 
        VecN->y = (I.y - Cir.y) / radius;
        VecN->z = (I.z - Cir.z) / radius; 

        //fprintf (stderr, "%.3f %.3f %.3f\n", VecN->x, VecN->y, VecN->z); 

        // Calculate Vector L
        VecL->x = (Lit.x - I.x); 
        VecL->y = (Lit.y - I.y); 
        VecL->z = (Lit.z - I.z);

        normalize_vector (VecL);

        // Calculate Vector R
        VecR->x = VecL->x - ( 2.0 * dot(VecN, VecL) * VecN->x );
        VecR->y = VecL->y - ( 2.0 * dot(VecN, VecL) * VecN->y );
        VecR->z = VecL->z - ( 2.0 * dot(VecN, VecL) * VecN->z );

        //normalize_vector (VecR); 
        //fprintf (stderr, "%.3f\n", VecR.x); 
        
        //return true;
    }

    //else
        //return false;   
} 

color_c do_lighting (color_d obj_color, vector_t VecD, vector_t VecN, 
                     vector_t VecL, vector_t VecR)
{
    const int n =  100; 
    const int Cs =   2; // Specular color

    color_d fcol; 
    color_c outcol;   
    
    fcol.r = 0.10 * obj_color.r; 
    fcol.g = 0.10 * obj_color.g;
    fcol.b = 0.10 * obj_color.b;

    if ( dot(&VecN, &VecL) > 0 ) 
    {
        fcol.r += dot(&VecN, &VecL) * obj_color.r * 0.9; 
        fcol.g += dot(&VecN, &VecL) * obj_color.g * 0.9; 
        fcol.b += dot(&VecN, &VecL) * obj_color.b * 0.9;
    }

    vector_t VecV; 
    VecN.x *= -1;  
    VecN.y *= -1;
    VecN.z *= -1;
    VecV.x = 2.0 * -VecN.x * dot(&VecN, &VecD); 
    VecV.y = 2.0 * -VecN.y * dot(&VecN, &VecD);
    VecV.z = 2.0 * -VecN.z * dot(&VecN, &VecD);

    //fprintf (stderr, "%.3f, %.3f, %.3f\n", VecV.x, VecV.y, VecV.z); 
 
    if ( dot(&VecR, &VecV) < 0 ) 
    {
        fcol.r += pow ( dot(&VecR, &VecV), n ); // VecN <--> VecD
        fcol.g += pow ( dot(&VecR, &VecD), n );
        fcol.b += pow ( dot(&VecR, &VecD), n ); 
    }
 
    //fprintf (stderr, "%.3f %.3f %.3f\n", fcol.r, fcol.g, fcol.b); 

    outcol.r = (unsigned char) (fcol.r * 255.0);
    outcol.g = (unsigned char) (fcol.g * 255.0);
    outcol.b = (unsigned char) (fcol.b * 255.0);

    //fprintf (stderr, "%hhu\n", outcol.r); 

    return outcol; 
    
}

double dot (vector_t *v1, vector_t *v2)
{
    double result;
    result = (v1->x * v2->x) + (v1->y * v2->y) + (v1->z * v2->z); 
    //fprintf (stderr, "%.3f\n", result); 
    return result; 
     
}   

void print_ppm_header (FILE *out)
{
    fprintf (out, "P6\n%d %d\n%d\n", width, height, maxV); 
}

void normalize_vector (vector_t *v)
{
    double length; 
    
    length = sqrt ( pow(v->x, 2) + pow(v->y, 2) + pow(v->z, 2) ); 
     
    v->x /= length;
    v->y /= length;
    v->z /= length; 
}




#include <stdio.h>
#include <stdbool.h>
#include <math.h>
//#include "vector.h"

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b; 
}  COLOR_C; 

typedef struct {
  double r;
  double g;
  double b; 
}  COLOR_D; 

typedef struct {
  double x; 
  double y; 
  double z; 
}  VEC_T; 

typedef struct {
  double x; 
  double y; 
  double z; 
}  RAY_T;

/*typedef struct {
  double x; 
  double y; 
  double z; 
}  VEC_PT;*/

/*typedef struct {
   VEC_PT origin; 
   VEC_PT dir;  
}  RAY_T;*/

typedef struct {
  RAY_T D;
  RAY_T N; 
  RAY_T L; 
  RAY_T R;
  RAY_T V;
  VEC_T center;
  VEC_T intersect_point;
  int radius;
  COLOR_D sphere_color; 
  COLOR_D final_color;
  COLOR_C write_color;   
}  SPHERE_T; 

typedef struct {
  VEC_T eye;
  VEC_T light;
}  WORLD_T;

  const int height = 500; 
  const int width = 500;
  const int maxValue = 255;
    
     
    void normalize_vector (RAY_T *ray);
    bool intersect_sphere (SPHERE_T *sphere, WORLD_T *world);
    void find_intersection (double);  
    void print_ppm_header (FILE *out);   
    void do_lighting (SPHERE_T *sphere, WORLD_T *world);  
    double dot (RAY_T *r1, RAY_T *r2);
    void translate_space_dimension (RAY_T *v, int *, int *);

    


int main (void)
{  
    int x, y;  
    bool intersect; 

    SPHERE_T sphere;
    sphere.radius   =  3; 
    sphere.center.x =  0; 
    sphere.center.y =  0; 
    sphere.center.z = 10;
    sphere.sphere_color.r = 1; 
    sphere.sphere_color.g = 0; 
    sphere.sphere_color.b = 0;
    sphere.write_color.r = 255; 
    sphere.write_color.g =   0; 
    sphere.write_color.b =   0;   
    
    WORLD_T  world;
    world.eye.x   =   0;
    world.eye.y   =   0; 
    world.eye.z   =   0; 
    world.light.x = -10; 
    world.light.y =  10;
    world.light.z =   5; 
      
    FILE *out;
    //out = fopen ("output.ppm", "w"); 
    out = stdout; 
    print_ppm_header (out); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < height; ++y ) 
    {
         for ( x = 0; x < width; ++x ) 
         {  
              // Convert 2D space to 3D space
              translate_space_dimension (&sphere.D, &x, &y);
               
              // Normalize 
              normalize_vector (&sphere.D);
              //fprintf (stderr, "%.3f\n", sphere.D.x);  
              
              intersect = intersect_sphere (&sphere, &world); 
              
              //fprintf (stderr, "%.3f\n", t); 
              if (intersect)
              {
                  do_lighting (&sphere, &world); 
                  fprintf (out, "%c%c%c", sphere.write_color.r, 
                           sphere.write_color.g, sphere.write_color.b); 
                    
                  //fprintf (stderr, "%c\n", outcol.r);
              }
              else
                  fprintf (out, "%c%c%c", 0, 0, 0); 
         }
    } 
    
    fclose (out); 

    return 0; 
}

void translate_space_dimension (RAY_T *v, int *x, int *y)
{
    v->x = -0.5 + ( (double) *x / width  ); 
    v->y =  0.5 - ( (double) *y / height ); 
    v->z =  1.0;
}   

bool intersect_sphere (SPHERE_T *sphere, WORLD_T *world)
{
    double A, B, C, dis, t0, t1, t;  
      
    A = 1; 
    B = 2 * ( sphere->D.x * (world->eye.x - sphere->center.x) + 
              sphere->D.y * (world->eye.y - sphere->center.y) + 
              sphere->D.z * (world->eye.z - sphere->center.z) ); 
    C =     ( pow(world->eye.x - sphere->center.x, 2) + 
              pow(world->eye.y - sphere->center.y, 2) + 
              pow(world->eye.z - sphere->center.z, 2) ) -
              pow(sphere->radius, 2);  

    // Calculate the discriminant, the part of the quadratic formula inside the square root
    dis = B * B - (4.0 * A * C); 

    if (dis >= 0.0 && A != 0.0)
    {
        t0  = ( -B - sqrt (dis) ) / 2.0 * A; 
        t1  = ( -B + sqrt (dis) ) / 2.0 * A;
        
        if (t0 < t1 && t0 > 0.0)
            t = t0;

        else if (t1 < t0 && t1 > 0.0)
            t = t1;

        // Calculate intersect points, points where ray and surface of sphere intersect
        sphere->intersect_point.x = world->eye.x + sphere->D.x * t; 
        sphere->intersect_point.y = world->eye.y + sphere->D.y * t;
        sphere->intersect_point.z = world->eye.z + sphere->D.z * t;

        // Calculate Ray N
        sphere->N.x = (sphere->intersect_point.x - sphere->center.x) / sphere->radius; 
        sphere->N.y = (sphere->intersect_point.y - sphere->center.y) / sphere->radius; 
        sphere->N.z = (sphere->intersect_point.z - sphere->center.z) / sphere->radius; 

        // Calculate Ray L
        sphere->L.x = world->light.x - sphere->intersect_point.x; 
        sphere->L.y = world->light.y - sphere->intersect_point.y;
        sphere->L.z = world->light.z - sphere->intersect_point.z;

        normalize_vector (&sphere->L);
        
        // Calculate Ray R
        sphere->R.x = sphere->L.x - ( 2 * dot(&sphere->N, &sphere->L) * sphere->N.x ); 
        sphere->R.y = sphere->L.y - ( 2 * dot(&sphere->N, &sphere->L) * sphere->N.y );
        sphere->R.z = sphere->L.z - ( 2 * dot(&sphere->N, &sphere->L) * sphere->N.z );

        // Calculate Ray V, whose formula is given as V = 2 * N * dot(-N, D)
        sphere->V.x = 2 * sphere->N.x * dot(&sphere->N, &sphere->D); 
        sphere->V.y = 2 * sphere->N.y * dot(&sphere->N, &sphere->D);
        sphere->V.z = 2 * sphere->N.z * dot(&sphere->N, &sphere->D); 

        if (t > 0)
            return true; 
    }

    else
        return false;  
} 

void do_lighting (SPHERE_T *sphere, WORLD_T *world)
{
    const int n =  100; 
    const int Cs =   2;  // Specular color   

    sphere->final_color.r = 0.10 * sphere->sphere_color.r;
    sphere->final_color.g = 0.10 * sphere->sphere_color.g;
    sphere->final_color.b = 0.10 * sphere->sphere_color.b;

    if ( dot(&sphere->N, &sphere->L) > 0 ) 
    {
        sphere->final_color.r += dot(&sphere->N, &sphere->L) * sphere->sphere_color.r; 
        sphere->final_color.g += dot(&sphere->N, &sphere->L) * sphere->sphere_color.g; 
        sphere->final_color.b += dot(&sphere->N, &sphere->L) * sphere->sphere_color.b; 
    }
     
    if ( dot(&sphere->R, &sphere->D) > 0 )  
    {
        sphere->final_color.r += pow ( dot(&sphere->R, &sphere->D), n ); 
        sphere->final_color.g += pow ( dot(&sphere->R, &sphere->D), n ); 
        sphere->final_color.b += pow ( dot(&sphere->R, &sphere->D), n ); 
    }

    if (sphere->final_color.r > 1.0) 
        sphere->final_color.r = 1.0; 

    if (sphere->final_color.g > 1.0) 
        sphere->final_color.g = 1.0; 
 
    if (sphere->final_color.b > 1.0) 
        sphere->final_color.b = 1.0;

    sphere->write_color.r = (unsigned char) (sphere->final_color.r * 255);
    sphere->write_color.g = (unsigned char) (sphere->final_color.g * 255);
    sphere->write_color.b = (unsigned char) (sphere->final_color.b * 255);
}

double dot (RAY_T *r1, RAY_T *r2)
{
    double result;
    result = (r1->x * r2->x) + 
             (r1->y * r2->y) + 
             (r1->z * r2->z); 
    return result;    
}  

void print_ppm_header (FILE *out)
{
    fprintf (out, "P6\n%d %d\n%d\n", width, height, maxValue); 
}

void normalize_vector (RAY_T *ray)
{
    double length; 
    
    length = sqrt ( pow(ray->x, 2) + pow(ray->y, 2) + pow(ray->z, 2) ); 
     
    ray->x /= length;
    ray->y /= length;
    ray->z /= length; 
}




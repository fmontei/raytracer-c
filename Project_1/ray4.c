#include <stdio.h>
#include <stdbool.h>
#include <math.h>
//#include "vector.h"

// Structure that contains color values needed for printing to image file
typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b; 
}  COLOR_C; 

// Structure that contains color values needed for performing light calculations
typedef struct {
  double r;
  double g;
  double b; 
}  COLOR_D; 

// Structure that differentiates a point from a ray
typedef struct {
  double x; 
  double y; 
  double z; 
}  VEC_PT; 

typedef struct {
  VEC_PT origin;
  VEC_PT dir;
}  RAY_T;

// Sphere structure
typedef struct {
  VEC_PT center;
  double radius;
  COLOR_D sphere_color; 
  COLOR_D final_color;
  COLOR_C write_color;   
}  SPHERE_T; 

// Globally defined PPM dimensions and PPM maximum color value
const int height   = 500; 
const int width    = 500;
const int maxValue = 255;  

/* FUNCTION PROTOTYPES */     
void normalize_vector (VEC_PT *vec);
bool intersect_sphere (RAY_T *, SPHERE_T *, VEC_PT *, VEC_PT *, VEC_PT *, 
                                             VEC_PT *, VEC_PT *, VEC_PT *);
void find_intersection (double);  
void print_ppm_header (FILE *);   
void do_lighting (SPHERE_T *sphere, RAY_T *ray, VEC_PT *VecN, VEC_PT *VecL, 
                                                                VEC_PT *VecR);
double dot (VEC_PT *, VEC_PT *);
void translate_space_dimension (RAY_T *, double *, double *);  

int main (void)
{  
    int x, y;  
    bool intersect; 
   
    COLOR_C background;
    background.r = 0;
    background.g = 0;
    background.b = 0;

    RAY_T ray;
    ray.origin.x = 0;
    ray.origin.y = 0;
    ray.origin.z = 0;

    VEC_PT eye, light_src;
    eye.x       =   0; 
    eye.y       =   0; 
    eye.z       =   0; 
    light_src.x = -10;
    light_src.y =  10;
    light_src.z =   5; 

    VEC_PT VecN, VecL, VecR, int_pt;

    // Define the sphere's initial characteristics
    SPHERE_T sphere;
    sphere.radius         =  3; 
    sphere.center.x       =  0; 
    sphere.center.y       =  0; 
    sphere.center.z       = 10;
    sphere.sphere_color.r =  1; 
    sphere.sphere_color.g =  0; 
    sphere.sphere_color.b =  0;    
      
    FILE *out;
    out = fopen ("output.ppm", "w"); 
    //out = stdout; 

    print_ppm_header (out); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < height; ++y ) 
    {
         for ( x = 0; x < width; ++x ) 
         {  
              // Convert 2D space to 3D space
              // translate_space_dimension (&ray, &x, &y);

              ray.dir.x = -0.5 + ( (double) x / width  ); 
              ray.dir.y =  0.5 - ( (double) y / height ); 
              ray.dir.z =  1.0;
               
              // Normalize the direction ray
              normalize_vector (&ray.dir);
              
              // Calculate where the direction ray intersects the sphere
              intersect = intersect_sphere (&ray, &sphere, &eye, &light_src, &int_pt, 
                                                                &VecN, &VecL, &VecR); 
             
              // If the ray intersects, print a pixel at that location
              if (intersect)
              {
                  // Calculate ambient, diffuse, and spectral lighting, 
                  // all three of which are combined within sphere.write_color
                  do_lighting (&sphere, &ray, &VecN, &VecL, &VecR);
                  fprintf (out, "%c%c%c", sphere.write_color.r, 
                           sphere.write_color.g, sphere.write_color.b); 
              }
              // Else if ray does not intersect sphere, print background color
              else
                  fprintf (out, "%c%c%c", background.r, background.g, background.b); 
         }
    } 
    
    fclose (out); 

    return 0; 
}

void translate_space_dimension (RAY_T *ray, double *x, double *y)
{
    ray->dir.x =  -0.5 + (*x / width); 
    ray->dir.y =   0.5 - (*y / height); 
    ray->dir.z =   1.0;
}   

bool intersect_sphere (RAY_T *ray, SPHERE_T *sphere, VEC_PT *eye, VEC_PT *light_src, 
                            VEC_PT *int_pt, VEC_PT *VecN, VEC_PT *VecL, VEC_PT *VecR)
{
    double A, B, C, dis, t0, t1, t;  
    const int Cs = 2;  // Specular color   
      
    A = 1; 
    B = 2 * ( ray->dir.x * (eye->x - sphere->center.x) + 
              ray->dir.y * (eye->y - sphere->center.y) + 
              ray->dir.z * (eye->z - sphere->center.z) ); 
    C =     ( pow(eye->x - sphere->center.x, 2) + 
              pow(eye->y - sphere->center.y, 2) + 
              pow(eye->z - sphere->center.z, 2) ) -
              pow(sphere->radius, 2);  

    // Calculate the discriminant, the part of the quadratic formula inside the square root
    dis = B * B - (4 * A * C); 

    if (dis >= 0 && A != 0)
    {
        t0  = ( -B - sqrt (dis) ) / 2 * A; 
        t1  = ( -B + sqrt (dis) ) / 2 * A;
        
        if (t0 < t1 && t0 > 0)
            t = t0;

        else if (t1 < t0 && t1 > 0)
            t = t1;

        /////////////////////////
        // VECTOR CALCULATIONS //
        /////////////////////////
 
        // Calculate intersect points, points where ray and surface of sphere intersect
        int_pt->x = eye->x + ray->dir.x * t; 
        int_pt->y = eye->y + ray->dir.y * t;
        int_pt->z = eye->z + ray->dir.z * t;

        // Calculate Ray N
        VecN->x = (int_pt->x - sphere->center.x) / sphere->radius; 
        VecN->y = (int_pt->y - sphere->center.y) / sphere->radius; 
        VecN->z = (int_pt->z - sphere->center.z) / sphere->radius; 

        // Calculate Ray L
        VecL->x = light_src->x - int_pt->x; 
        VecL->y = light_src->y - int_pt->y;
        VecL->z = light_src->z - int_pt->z;

        normalize_vector (VecL);
        
        // Calculate Ray R
        VecR->x = VecL->x - ( Cs * dot(VecN, VecL) * VecN->x ); 
        VecR->y = VecL->y - ( Cs * dot(VecN, VecL) * VecN->y );
        VecR->z = VecL->z - ( Cs * dot(VecN, VecL) * VecN->z );

        normalize_vector (VecR); 
        
        return true; 
    }

    else
        return false;  
} 

void do_lighting (SPHERE_T *sphere, RAY_T *ray, VEC_PT *VecN, VEC_PT *VecL, VEC_PT *VecR)
{
    const int n =  100; 

    // Add in ambient lighting 
    sphere->final_color.r = 0.10 * sphere->sphere_color.r; 
    sphere->final_color.g = 0.10 * sphere->sphere_color.g;
    sphere->final_color.b = 0.10 * sphere->sphere_color.b;

   // Cosine theta = dot(N, L)
    // If cosine theta is greater than 0, add in diffuse lighting
    if ( dot(VecN, VecL) > 0 ) 
    {
        sphere->final_color.r += dot(VecN, VecL) * sphere->sphere_color.r; 
        sphere->final_color.g += dot(VecN, VecL) * sphere->sphere_color.g; 
        sphere->final_color.b += dot(VecN, VecL) * sphere->sphere_color.b; 
    }
    
    // Add in specular lighting
    if ( dot(VecR, &ray->dir) > 0 )  
    {
        sphere->final_color.r += pow ( dot(VecR, &ray->dir), n ); 
        sphere->final_color.g += pow ( dot(VecR, &ray->dir), n );  
        sphere->final_color.b += pow ( dot(VecR, &ray->dir), n );  
    }

    if (sphere->final_color.r > 1)
        sphere->final_color.r = 1; 

    if (sphere->final_color.g > 1)
        sphere->final_color.g = 1; 

    if (sphere->final_color.b > 1)
        sphere->final_color.b = 1; 

    // Convert the color from type double to type unsigned char
    sphere->write_color.r = (unsigned char) (sphere->final_color.r * 255);
    sphere->write_color.g = (unsigned char) (sphere->final_color.g * 255);
    sphere->write_color.b = (unsigned char) (sphere->final_color.b * 255); 
} 

double dot (VEC_PT *v1, VEC_PT *v2)
{
    double result;
    result = (v1->x * v2->x) + 
             (v1->y * v2->y) + 
             (v1->z * v2->z); 
    return result;    
}  

void print_ppm_header (FILE *out)
{
    fprintf (out, "P6\n%d %d\n%d\n", width, height, maxValue); 
}

void normalize_vector (VEC_PT *vec)
{
    double length; 
    
    length = sqrt ( vec->x * vec->x + vec->y * vec->y + vec->z * vec->z); 
     
    vec->x /= length;
    vec->y /= length;
    vec->z /= length; 
}


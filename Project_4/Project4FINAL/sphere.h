#ifndef SPHERE_H
#define SPHERE_H

#include <iostream>
#include <cmath>
#include "vector.h"
#include "typedef.h"
#include "object.h"

using namespace std; 

class Sphere : public Object // Example of inheritance
{
private:
    double radius; 
    Vector center; 

public:  
    Sphere() { radius = 0; center = {0, 0, 0}; }

    Sphere(const double &r, const double &x, const double &y, 
        const double &z) 
    {  
       if (r < 0) 
          cout << "Error: radius cannot be negative\n";

       radius = abs(r);
       center = {x, y, z}; 
    }
    
    void set (const double &r, const double &x, const double &y, 
        const double &z)  
    {
       if (r < 0)
          cout << "Error: radius cannot be negative\n";
       
       radius = abs(r); 
       center = {x, y, z}; 
    }
    
    /* In order to avoid making the derived Class Sphere abstract, the 
     * virtual function in Object must be overridden */
    
    bool intersect (RAY_T, Vector *, Vector *, double &);
};

#endif

#include <iostream>
#include <cmath>
#include "typedef.h"
#include "vector.h"
#include "light.h"
#include "sphere.h"
#include "object.h"

PIXEL_T operator*(const PIXEL_T &, const PIXEL_T &);

PIXEL_T operator+(const PIXEL_T &, const double &);

PIXEL_T checkerboard (Object *, Vector); 

PIXEL_T Light::setLight (const RAY_T &Ray, Vector &IntersectPoint, 
        Vector &Normal, Object **objects, const int &closestObject)
{
   const int n = 100;               
   const double AmbientFactor = 0.1;    
   Vector L, R;              
   PIXEL_T FinalColor, InitialColor = objects[closestObject]->getColor();
   
   /* If the object is a plane, checkerboard is called; else
    * InitialColor remains initialized to object->getColor(); */
   
   if (objects[closestObject]->getType() == 'p')
      InitialColor = checkerboard(objects[closestObject], IntersectPoint);  
  
   PIXEL_T tempColor = {AmbientFactor, AmbientFactor, AmbientFactor};
   FinalColor = InitialColor * tempColor; 

   L = this -> location - IntersectPoint; 
   L = L.normalize();   
   
   if (!shadowTest (objects, closestObject, IntersectPoint, L))
   {  
      if (Normal.dot(L) > 0)
      {
         FinalColor = (FinalColor + Normal.dot(L)) * InitialColor;
         
         Vector tempV(2 * Normal.dot(L), 2 * Normal.dot(L), 2 * Normal.dot(L));
         R = L - (Normal * tempV); 
         R = R.normalize(); 

         if (R.dot(Ray.dir) > 0)
            FinalColor = FinalColor + pow (R.dot(Ray.dir), n); 
      }
   }
   
   return FinalColor; 
}

PIXEL_T operator*(const PIXEL_T &color1, const PIXEL_T &color2)
{
   PIXEL_T result;
   
   result.r = color1.r * color2.r;
   result.g = color1.g * color2.g;
   result.b = color1.b * color2.b;
   
   return result;
} 

PIXEL_T operator+(const PIXEL_T &color, const double &value)
{
   PIXEL_T result;
   
   result.r = color.r + value; 
   result.g = color.g + value;
   result.b = color.b + value;
     
   return result;
} 

bool Light::shadowTest (Object **&objectsArray, const int &closestObject, 
                Vector &IntersectPoint, Vector &L)
{
   int i;
   Vector dummyNormal;
   double t;
   
   RAY_T ShadowRay;
   ShadowRay.origin = IntersectPoint;
   ShadowRay.dir = L;

   for ( i = 0; i < OBJECT_NUM; ++i )
   {
      if (i != closestObject)
      {
         if (objectsArray[i]->intersect(ShadowRay, &IntersectPoint, 
                 &dummyNormal, t))
            return true;
      }
   }
   
   return false; 
}

PIXEL_T Light::checkerboard (Object *&plane, Vector &intersectPoint)
{
   if (intersectPoint.planeFloor() == 1)
      return plane->getColor(); 
   
   else
      return plane->getColor2(); 
}

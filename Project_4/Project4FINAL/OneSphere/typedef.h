#ifndef TYPEDEF_H
#define TYPEDEF_H

#define OBJECT_NUM 1

#include "vector.h"

typedef struct {
  double r;
  double g;
  double b;
} PIXEL_T;

typedef struct {
  const int width;
  const int height;
}  IMG_T; 

typedef struct {
  Vector origin;
  Vector dir;
}  RAY_T; 

#endif

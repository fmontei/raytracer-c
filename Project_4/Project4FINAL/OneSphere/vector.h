#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

class Vector 
{
private:
   double x;
   double y;
   double z;
   
public:
   Vector();                                          

   Vector(const double& startX, const double &startY, const double &startZ) 
   {
      this -> x = startX; this -> y = startY; this -> z = startZ;
   }

   /* Overloading Operators */
   Vector  operator-(const Vector &);     // Vector-Vector operations
   Vector  operator+(const Vector &);
   Vector  operator*(const Vector &);
   
   Vector  operator/(const double &);     // Vector-double operations;
   double  operator*(const double &); 
   double  operator-(const double &);  

   /* Mutator Method */
   void   set (const double &, const double &, const double &); 

   /* Vector Methods */
   double  dot (const Vector &);
   Vector  normalize (void);
   int     planeFloor (void);
};

#endif 

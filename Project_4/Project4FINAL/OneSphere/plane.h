/* 
 * File:   plane.h
 * Author: fmontei
 *
 * Created on June 16, 2013, 12:50 AM
 */

#ifndef PLANE_H
#define	PLANE_H

#include <iostream>
#include "vector.h"
#include "typedef.h"
#include "object.h"

class Plane : public Object
{
private:
   Vector Normal;
   double D;
   
public:
   Plane() { D = 0; Normal = {0, 0, 0}; } 
   
   /* All the values that make up a plane can be negative */
   Plane(const double &startD, const double &x, const double &y, 
           const double &z) 
   {
      D = startD; 
      Normal = {x, y, z};
   }
   
   void set (const double &newD, const double &x, const double &y, 
           const double &z) 
   {
      D = newD; 
      Normal = {x, y, z}; 
   }
   
   bool intersect (RAY_T, Vector *, Vector *, double &);  
};

#endif	/* PLANE_H */


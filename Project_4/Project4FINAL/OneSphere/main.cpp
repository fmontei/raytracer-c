#include <iostream>
#include <fstream>
#include "light.h"
#include "plane.h"
#include "typedef.h"             
#include "sphere.h"
#include "vector.h" 
#include "object.h"

using namespace std; 

/* Compile using -std=c++0x */
PIXEL_T trace (RAY_T &, Object **, Light &);

void aspectRatio (const IMG_T &, double &, double &);

void init (Object **);                  // Pass double pointer by reference?

int main (void) 
{    
   int x, y; 
   double smallerSide, ratio; 
    
   IMG_T image = {500, 500};             // Structures
   PIXEL_T writeColor;   
   RAY_T ray;
    
   Light light(-10.0, 10.0, 5.0);        // Classes
   Vector normal, intersectPoint;
   
   Object **objects;                     // Array of pointers
   objects = new Object*[OBJECT_NUM];    // Dynamically allocate memory
   init(objects);                        // Initialize objects 
    
   ofstream imageFile("img.ppm", ios_base::binary);

   if (!imageFile)
      cout << "Error: image file cannot be created" << endl;  
   
   aspectRatio(image, smallerSide, ratio);

   imageFile << "P6" << endl << image.width << " " << image.height << endl
             << "255" << endl;  

   for ( y = 0; y < image.height; ++y )
   {
      // \r keeps the print statement on one line: works better w/ stderr
      fprintf (stderr, "\rRendering image...%5.2f%%", 
               100.0 * y / (image.height - 1));
       
      for ( x = 0; x < image.width; ++x )
      {
         ray.origin.set(0.0, 0.0, 0.0);
         
         if (image.width > image.height)
            ray.dir.set(-ratio + ((double) x * smallerSide), 
                           0.5 - ((double) y * smallerSide),
             	           1.0                              );
         
         else 
            ray.dir.set( -0.5 + ((double) x * smallerSide), 
                        ratio - ((double) y * smallerSide),
             	          1.0                              );
          
     	 ray.dir = ray.dir.normalize(); 
          
         writeColor = trace (ray, objects, light);
          
         if (writeColor.r > 1) writeColor.r = 1; 
         if (writeColor.g > 1) writeColor.g = 1;
         if (writeColor.b > 1) writeColor.b = 1;
             
         imageFile << static_cast<unsigned char> (writeColor.r * 255) <<
     	              static_cast<unsigned char> (writeColor.g * 255) <<
 		      static_cast<unsigned char> (writeColor.b * 255);
      }
   }  
    
   fprintf (stderr, "\nRendering complete\nImage saved as img.ppm\n");
    
   delete objects; 
   imageFile.close(); 
    
   return 0; 
}

void init (Object **objects)
{
   Sphere *sphere1 = new Sphere; 
   sphere1 -> set(3.0, 0.0, 0.0, 10.0);            // Radius then location
   sphere1 -> setColor1((PIXEL_T) {0.8, 0, 0});
   sphere1 -> setType('s');
   
   objects[0] = const_cast<Sphere *> (sphere1);   // const_cast to make const 
                                                   // pointer to sphere/plane
}

void aspectRatio (const IMG_T &image, double &denominator, double &ratio)
{
    int numerator; 

    if (image.width > image.height) 
    {
        denominator = image.height; 
        numerator   = image.width; 
    }
 
    else
    {
        denominator = image.width;
        numerator   = image.height; 
    }

   ratio = ((double) numerator / denominator) / 2; 
   
   denominator = 1.0 / denominator; 
}

PIXEL_T trace (RAY_T &Ray, Object **objects, Light &light)
{
   int i;
   int closestObject = -1; 
   double t; 
   double closest_t = 1000.0;
   static const double EPS = 0.01;             
   Vector IntersectPoint, Normal;
   Vector closestIntersectPt, closestNormal;
   PIXEL_T finalColor = {0, 0, 0};
   
   for ( i = 0; i < OBJECT_NUM; ++ i )
   {
      if (objects[i] -> intersect(Ray, &IntersectPoint, &Normal, t))
      {
         if (t > EPS && t < closest_t)
         {
            closest_t = t;
            closestNormal = Normal;
            closestIntersectPt = IntersectPoint;
            closestObject = i;
         }
      }
   }
   
   if (closestObject >= 0)
   {
      finalColor = light.setLight(Ray, closestIntersectPt, closestNormal,  
              objects, closestObject);
   }
   
   return finalColor; 
}


#include "plane.h"
#include "typedef.h"
#include "vector.h"

/* t is passed by reference but the & symbol means it doesn't need to be
 * dereferenced every time its value is changed */

bool Plane::intersect(RAY_T Ray, Vector *intersectPoint, Vector *Normal, 
        double &t)
{
   if (Ray.dir.dot(this -> Normal) == 0) 
      return false;
    
   /* Using this -> Normal means the parameter doesn't need to be
    * passed as *Normal, as Vector::dot expects a normal parameter */
   
   else        
      t = -1 * (Ray.origin.dot(this -> Normal) + this -> D / 
                Ray.dir.dot(this -> Normal));

   if (t < 0) 
      return false;
   
   Vector temp(t, t, t);
   *intersectPoint = (Ray.origin + Ray.dir) * temp; 
   *Normal = this -> Normal; 
   
   return true; 
}

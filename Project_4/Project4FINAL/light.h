#ifndef LIGHT_H
#define LIGHT_H

#include "vector.h"
#include "typedef.h"
#include "object.h"

class Light 
{
private: 
    Vector location;
    PIXEL_T checkerboard (Object *&, Vector &);
    bool    shadowTest (Object **&, const int &, Vector &, Vector &);

public:
    Light(); 
   
    Light (double x, double y, double z) :
        location({x, y, z}) {} 
    
    PIXEL_T setLight (const RAY_T &, Vector &, Vector &, Object **, 
                const int &);  
};

#endif


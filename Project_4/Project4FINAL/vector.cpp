#include "vector.h"
#include <cmath> 
#include <iostream>

// Note: this statements are not necessary

/* Default constructor */
Vector::Vector() 
{
   x = 0;
   y = 0; 
   z = 0;
}

void Vector::set (const double &x, const double &y, const double &z)
{
   this -> x = x;
   this -> y = y;
   this -> z = z; 
}

//========Vector-Vector operations=========//

Vector Vector::operator-(const Vector &otherVector)
{
    Vector result; 

    result.set(this -> x - otherVector.x, 
               this -> y - otherVector.y,
    	       this -> z - otherVector.z);

    return result; 
}  

Vector Vector::operator+(const Vector &otherVector)
{
    this -> x += otherVector.x; 
    this -> y += otherVector.y;
    this -> z += otherVector.z;

    return *this;
} 

Vector Vector::operator*(const Vector &otherVector)
{
    Vector result; 

    result.set(this -> x * otherVector.x, 
               this -> y * otherVector.y,
               this -> z * otherVector.z);

    return result; 
} 
 
//============Vector Operations============//

double Vector::dot (const Vector &otherVector)
{
    double result;

    result = this -> x * otherVector.x +
	     this -> y * otherVector.y +
	     this -> z * otherVector.z;

    return result;
} 

Vector Vector::normalize (void)
{
    double length;
    Vector result; 

    length = sqrt (this -> x * this -> x + 
                   this -> y * this -> y + 
                   this -> z * this -> z);

    result.set(this -> x / length,
               this -> y / length,
               this -> z / length); 

    return result; 
}

int Vector::planeFloor (void) 
{
   int value;
   return value = (((int) floor(this -> x) + 
                     (int) floor(this -> y) + 
                     (int) floor(this -> z)) & 1);
}

//========Vector-Double operations=========//

double Vector::operator*(const double &factor)
{
    return (this -> x + this -> y + this -> z) * factor;  
}

double Vector::operator-(const double &number)
{
    return (this -> x + this -> y + this -> z) - number; 
}

Vector Vector::operator/(const double &number)
{
   if (number != 0) // Prevent division by zero
   {
      Vector quotient(this -> x / number, 
                      this -> y / number, 
                      this -> z / number); 
      
      return quotient;
   }
   
   else
      return *this;
}

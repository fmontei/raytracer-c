#include "typedef.h"
#include "sphere.h"
#include "vector.h"
#include <iostream>
#include <cmath> 

using namespace std;
  
bool Sphere::intersect (RAY_T ray, Vector *intersectPoint, Vector *normal, 
        double &t) 
{
   double B, C, discriminant, t0, t1;
   double squareRadius = radius * radius; 

   /* The doubles must be on the right-hand side of the equation, because
    * only user-defined types can have overloaded operators, not built-in
    * types like int. So the "order of operation" must be vector * double */

   B = (ray.dir * (ray.origin - center)) * 2; 

   C = ((ray.origin - center) * (ray.origin - center)) - squareRadius;

   discriminant = B * B - (4 * C);

   if (discriminant < 0)
       return false;

   t0  = ( -B - sqrt (discriminant) ) / 2;  
   t1  = ( -B + sqrt (discriminant) ) / 2;
      
   if (t0 < 0 && t1 < 0) 
      return false;

   if (t0 < t1 && t0 > 0)
      t = t0;

   else
      t = t1;

   if (t1 < t0 && t1 > 0)
      t = t1;

   else
      t = t0;

   Vector temp(t, t, t); // Turn t into vector to utilize overloaded operator           
   *intersectPoint = (ray.origin + ray.dir) * temp; 
   
   *normal = (*intersectPoint - center) / radius;   
   
   return true;
}


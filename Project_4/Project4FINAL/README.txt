The main folder contains all the extra credit stuff: shadows, multiple spheres, a plane.
The subdirectory titled OneSphere produces the same sphere from Project 1.

Both "programs" (extra credit and non-) use the same layout. Extra C++ features not
required for this assignment include: inheritance, pure virtual functions, an abstract 
base class, and const references and references. static_cast and const_cast are also
used for experimentation. In object.h, the three get() methods are constant: e.g., 
get Color1 ( ... ) const;

To run either file, just type make. Both images are saved as img.ppm

Note: the files must be compiled using -std=c++0x because of the way in which
the constructors were coded. Instead of doing something like this:

Vector temp(1, 2, 3);
Sphere sphere(3, temp); 

where 3 denotes the radius and temp the sphere's location, I coded my program
such that it would accept the following:

Sphere sphere(3, 1, 2, 3); 

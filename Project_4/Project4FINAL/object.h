#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>
#include <cmath>
#include "typedef.h"

using namespace std; 

class Object 
{
private:
   PIXEL_T color, color2;
   char type;
   
            /* const after function prevents any parameters from being changed;
             * since accessor methods only return, this makes sense */
   
public: 
   PIXEL_T getColor  (void) const { return this -> color; }
   PIXEL_T getColor2 (void) const { return this -> color2; }
   char    getType   (void) const { return this -> type; }
   void    setType   (const char &newType) { type = newType; }
   
   PIXEL_T setColor (PIXEL_T newColor) 
   {
      if (newColor.r < 0 || newColor.g < 0 || newColor.b < 0)
         cout << "Error: color values must be positive" << endl;
      
      return newColor = (PIXEL_T) {abs(newColor.r), abs(newColor.g), 
              abs(newColor.b)};
   }
   
   void setColor1 (const PIXEL_T &newColor)  { color  = setColor (newColor); }
   void setColor2 (const PIXEL_T &newColor2) { color2 = setColor (newColor2); }
   
   /* This pure virtual function renders the Object Class abstract:
    * instances of it cannot be created. */
   
   virtual bool intersect (RAY_T, Vector *, Vector *, double &) = 0;  
};

#endif

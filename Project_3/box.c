#include "ray.h"
#include "vector.h"
#include <stdio.h>
#include <stdbool.h>

void swap (double *, double *); 

void swap (double *x, double *y)
{
    double temp;

    temp = *x;
    *x = *y;
    *y = temp;
}

bool intersect_box (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    int i;
    double t1, t2;
    const double EPS = 0.01; 
    double tnear = -2147000000;
    double tfar  =  2147000000;

    VEC_PT side;
    int swap_flag; 
    
    static VEC_PT box_norm[6] = { {-1,  0,  0}, {1, 0, 0}, 
  			           { 0, -1,  0}, {0, 1, 0}, 
      			           { 0,  0, -1}, {0, 0, 1} }; 
    
    double ray_dir[3] = {ray.dir.x, ray.dir.y, ray.dir.z};
    double ray_or[3] = {ray.or.x, ray.or.y, ray.or.z};  
		   
    for ( i = 0; i <= 2; ++i )
    {
	swap_flag = 0; 

        if (ray_dir[i] == 0) 
        {
            if (ray_or[i] < obj.geom.box.b1[i] || ray_or[i] > obj.geom.box.b2[i])
                return false;
        }

        else 
        {
            t1 = (obj.geom.box.b1[i] - ray_or[i]) / ray_dir[i];
            t2 = (obj.geom.box.b2[i] - ray_or[i]) / ray_dir[i];

            if (t1 > t2) 
            {
                swap (&t1, &t2);
                swap_flag = 1; 
            }

            if (t1 > tnear) 
            {
                tnear = t1; 

		if (swap_flag == 1)
 		    side = box_norm[2 * i + 1];

		else
		    side = box_norm[2 * i];
            }

            if (t2 < tfar) tfar = t2; 

            if (tnear > tfar) return false; 
 	        
            if (tfar < 0) return false; 
        }
   } 

   *norm = side; 
   *t = tnear; 

   /* tnear contains distance of ray */
   int_pt->x = ray.or.x + ray.dir.x * *t; 
   int_pt->y = ray.or.y + ray.dir.y * *t; 
   int_pt->z = ray.or.z + ray.dir.z * *t; 

   /*if ((int_pt->x - obj.geom.box.b1[0]) < EPS) 
       *norm = box_norm[0]; 

   else if ((int_pt->x - obj.geom.box.b2[0]) < EPS) 
       *norm = box_norm[1];

   else if ((int_pt->y - obj.geom.box.b1[1]) < EPS) 
       *norm = box_norm[2];

   else if ((int_pt->y - obj.geom.box.b2[1]) < EPS) 
       *norm = box_norm[3];

   else if ((int_pt->z - obj.geom.box.b1[2]) < EPS) 
       *norm = box_norm[4];

   else if ((int_pt->z - obj.geom.box.b2[2]) < EPS) 
       *norm = box_norm[5];*/

   return true;
}


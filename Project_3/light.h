#ifndef _LIGHT_H
#define _LIGHT_H

#include "ray.h"

COLOR_T do_lighting (IMG_T, SCENE_T, RAY_T, VEC_PT, VEC_PT, int);

bool shadow_test (IMG_T, OBJ_T [], RAY_T, VEC_PT, VEC_PT, int);

COLOR_T checkerboard (OBJ_T, VEC_PT);

COLOR_T get_color (OBJ_T, VEC_PT);

#endif

#ifndef _SPHERE_H
#define _SPHERE_H

#include "ray.h"

bool intersect_sphere (OBJ_T, RAY_T, VEC_PT *, VEC_PT *, double *);    

#endif

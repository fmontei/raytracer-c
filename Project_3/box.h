#ifndef _BOX_H
#define _BOX_H

#include "ray.h"
#include "vector.h"

bool intersect_box (OBJ_T, RAY_T, VEC_PT *, VEC_PT *, double *);

#endif 

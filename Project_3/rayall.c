#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <malloc.h> 
#include "ray.h" 
#include "vector.h"   
#include "init.h"

bool intersect_sphere (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    double B, C, discriminant, t0, t1;   
      
    B = 2 * (ray.dir.x * (ray.or.x - obj.geom.sphere.center.x) + 
             ray.dir.y * (ray.or.y - obj.geom.sphere.center.y) + 
             ray.dir.z * (ray.or.z - obj.geom.sphere.center.z)); 
    C =     ((ray.or.x - obj.geom.sphere.center.x) * (ray.or.x - obj.geom.sphere.center.x) + 
             (ray.or.y - obj.geom.sphere.center.y) * (ray.or.y - obj.geom.sphere.center.y) +
             (ray.or.z - obj.geom.sphere.center.z) * (ray.or.z - obj.geom.sphere.center.z) -
             (obj.geom.sphere.radius) * (obj.geom.sphere.radius));  

    /* Calculate the discriminant, the part of the quadratic formula inside the square root */
    discriminant = B * B - (4 * C); 

    if (discriminant < 0)
        return false; 
    
    t0  = (-B - sqrt (discriminant)) / 2;  // A is omitted since it is equal to 1 
    t1  = (-B + sqrt (discriminant)) / 2;
      
    /* If the ray is moving in the negative direction, return false */
    if (t0 < 0 && t1 < 0) 
        return false;  

    if (t0 < t1 && t0 > 0)
        *t = t0;

    else if (t1 < t0 && t1 > 0)
        *t = t1;

    /* Calculate intersect points, points where ray and surface of sphere intersect */
    int_pt->x = ray.or.x + ray.dir.x * *t; 
    int_pt->y = ray.or.y + ray.dir.y * *t;
    int_pt->z = ray.or.z + ray.dir.z * *t; 

    /* Calculate the normal */
    norm->x = (int_pt->x - obj.geom.sphere.center.x) / obj.geom.sphere.radius; 
    norm->y = (int_pt->y - obj.geom.sphere.center.y) / obj.geom.sphere.radius;
    norm->z = (int_pt->z - obj.geom.sphere.center.z) / obj.geom.sphere.radius;  

    return true; 
} 



bool intersect_plane (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    *norm = obj.geom.plane.norm; 

    if (vec_dot(obj.geom.plane.norm, ray.dir) == 0)
        return false;

    else 
        *t = -1 * (vec_dot(obj.geom.plane.norm, ray.or) + obj.geom.plane.D) / 
                   vec_dot(obj.geom.plane.norm, ray.dir);  
 
    if  (*t < 0)
        return false; 
   
    int_pt->x = ray.or.x + ray.dir.x * *t;  
    int_pt->y = ray.or.y + ray.dir.y * *t;
    int_pt->z = ray.or.z + ray.dir.z * *t;  

    return true; 
}

/* Sphere lighting */
COLOR_T get_color (OBJ_T obj, VEC_PT int_pt) 
{
    return obj.color;  
}

/* Plane lighting */
COLOR_T checkerboard (OBJ_T obj, VEC_PT int_pt) 
{
    if ((((int) floor(int_pt.x)) + ((int) floor(int_pt.y)) + ((int) floor(int_pt.z))) & 1)
        return obj.color; 

    else
        return obj.color2; 
}

bool shadow_test (OBJ_T objs[], RAY_T ray, VEC_PT int_pt, VEC_PT L, int closest_obj)
{
    int i; 
    VEC_PT norm; 
    double t; 

    /* The intersection point on the plane becomes the new ray origin */
    ray.or = int_pt; 
    ray.dir = L;     

    for ( i = 0; i < OBJ_NUM; ++i ) 
    {
        if (i != closest_obj) 
        {
            if (objs[i].intersect(objs[i], ray, &int_pt, &norm, &t) == true)
                return true;
        } 
    }  

    return false; 
}

COLOR_T do_lighting (SCENE_T scene, RAY_T ray, VEC_PT int_pt, VEC_PT norm, int c_obj)
{
    int i;
    const int n = 100;                 
    const double amb_factor = 0.10; 
    const double c1 = .002, c2 = .02, c3 = .2; 
    double atten, Dl;    
    VEC_PT L, R;             
    COLOR_T object_color, final_color;

    /* Function pointer that calls either checkerboard (for planes)
       or get_color (for spheres) */
    object_color = scene.objs[c_obj].color_ptr(scene.objs[c_obj], int_pt);   

    /* Ambient Lighting */
    final_color.r = amb_factor * object_color.r; 
    final_color.g = amb_factor * object_color.g;
    final_color.b = amb_factor * object_color.b;
 
    for ( i = 0; i < LIGHT_NUM; ++i )
    {
    	/* Calculate Vector L */
    	L = vec_subtract (scene.lights[i].source, int_pt); 

    	/* Light attenuation 
      	  |L| = magnitude of L = Dl */
    	Dl = vec_len (L); 	       
    	atten = 1 / (c1 * Dl * Dl + c2 * Dl + c3); 

    	L = vec_normalize (L);

    	if (shadow_test(scene.objs, ray, int_pt, L, c_obj) == false)
    	{
            if (vec_dot(norm, L) > 0) 
       	    {
	   	/* Diffuse lighting */
           	final_color.r += vec_dot(norm, L) * object_color.r * atten * scene.lights[i].intensity.r; 
           	final_color.g += vec_dot(norm, L) * object_color.g * atten * scene.lights[i].intensity.g; 
           	final_color.b += vec_dot(norm, L) * object_color.b * atten * scene.lights[i].intensity.b; 
    
           	R.x = L.x - 2 * vec_dot(norm, L) * norm.x; 
           	R.y = L.y - 2 * vec_dot(norm, L) * norm.y;
           	R.z = L.z - 2 * vec_dot(norm, L) * norm.z;

           	R = vec_normalize (R);

           	if ( vec_dot(R, ray.dir) > 0 )     
           	{
               	    /* Specular lighting */
               	    final_color.r += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.r; 
               	    final_color.g += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.g;  
               	    final_color.b += pow (vec_dot(R, ray.dir), n) * atten * scene.lights[i].intensity.b;  
               }
           }
        }
    }

    return final_color;  
} 

void swap (double *x, double *y)
{
    double temp;

    temp = *x;
    *x = *y;
    *y = temp;
}

bool intersect_box (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    int i;
    double t1, t2;
    const double EPS = 0.01; 
    double tnear = - 2147000000;
    double tfar  =   2147000000;
    
    static VEC_PT box_norm[6] = { {-1,  0,  0}, {1, 0, 0}, 
  			           { 0, -1,  0}, {0, 1, 0}, 
      			           { 0,  0, -1}, {0, 0, 1} }; 
    double ray_dir[3];
    double ray_or[3];

    ray_dir[0] = ray.dir.x;
    ray_dir[1] = ray.dir.y;
    ray_dir[2] = ray.dir.z;
    ray_or[0] = ray.or.x;
    ray_or[1] = ray.or.y;
    ray_or[2] = ray.or.z;  
			   
    for ( i = 0; i < 3; ++i )
    {
        if (ray_dir[i] == 0) 
        {
            if (ray_or[i] < obj.geom.box.b1[i] || ray_or[i] > obj.geom.box.b2[i])
                return false;
        }

        else 
        {
            t1 = (obj.geom.box.b1[i] - ray_or[i]) / ray_dir[i];
            t2 = (obj.geom.box.b2[i] - ray_or[i]) / ray_dir[i]; 

            if (t1 > t2) swap (&t1, &t2);

            if (t1 > tnear) tnear = t1; *t = tnear;

            if (t2 < tfar) tfar = t2;

            if (tnear > tfar) return false;
   
            if (tfar < 0) return false;
        }

   } 

   /* tnear contains distance of ray */
   int_pt->x = ray.or.x + ray.dir.x * *t; 
   int_pt->y = ray.or.y + ray.dir.y * *t; 
   int_pt->z = ray.or.z + ray.dir.z * *t; 

   if ((int_pt->x - obj.geom.box.b1[0]) < EPS) 
       *norm = box_norm[0]; 

   else if ((int_pt->x - obj.geom.box.b2[0]) < EPS) 
       *norm = box_norm[1];

   else if ((int_pt->y - obj.geom.box.b1[1]) < EPS) 
       *norm = box_norm[2];

   else if ((int_pt->y - obj.geom.box.b2[1]) < EPS) 
       *norm = box_norm[3];

   else if ((int_pt->z - obj.geom.box.b1[2]) < EPS) 
       *norm = box_norm[4];

   else if ((int_pt->z - obj.geom.box.b2[2]) < EPS) 
       *norm = box_norm[5];

   return true;
}



//////////////////////////////////////////
// Felipe Monteiro                      //
// cpsc102                              //
// Dr. Davis                            // 
// May 22, 2013                         //
// Project 2 -- Intermediate Ray Tracer //
//                                      //
// Ray tracer that uses more advanced   //
// coding techniques like function      //
// pointers, unions and multiple        //
// structures.This ray tracer can draw  //
// multiple spheres and planes. It      //
// calculates arbitrary aspect ratio,   //
// light attenuation and shadowing.     //
//////////////////////////////////////////


/* Ray-specific function prototypes */ 
void aspect_ratio (double *, double *); 

void print_ppm_header (FILE *);  

COLOR_T trace (SCENE_T, RAY_T, int);

void print_pixel (FILE *fp, COLOR_T **);

void define_objects (SCENE_T *);

COLOR_T** malloc_array (int, int); 
  
  
int main (void)
{  
    FILE *fp;
    int x, y;
    int depth = 0;  
    double denominator, ratio; 
    RAY_T ray; 
    SCENE_T scene, scene1; 
    IMG_T image;   
 
    image.height = 480, image.width = 640; 

    COLOR_T **pixel = malloc_array (WIDTH, HEIGHT); 

    fp = fopen ("img3.ppm", "w"); 

    init (&scene, &image);
  
    define_objects (&scene);                
     
    /* Function that calculates arbitrary aspect ratio */
    aspect_ratio (&denominator, &ratio);  

    fprintf (fp, "P6\n%d %d\n%d\n", image.width, image.height, MAXV); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < image.height; ++y ) 
    {
         for ( x = 0; x < image.width; ++x ) 
         {  
              ray.or = (VEC_PT) {0, 0, 0};

              // Convert ray.dir from 2D space to 3D space
              if (WIDTH > HEIGHT) 
              {
                  ray.dir.x = -ratio + ((double) x * denominator); 
                  ray.dir.y =  0.5   - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }
      
              else 
              {
                  ray.dir.x = -0.5   + ((double) x * denominator); 
                  ray.dir.y =  ratio - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }

              ray.dir = vec_normalize (ray.dir);      // Normalize ray direction
              
              pixel[x][y] = trace (scene, ray, depth); 
        
              if (pixel[x][y].r > 1) pixel[x][y].r = 1; 
    	      if (pixel[x][y].g > 1) pixel[x][y].g = 1; 
              if (pixel[x][y].b > 1) pixel[x][y].b = 1; 

              fprintf (fp, "%c%c%c", (unsigned char) (pixel[x][y].r * 255), 
              		             (unsigned char) (pixel[x][y].g * 255),
              		             (unsigned char) (pixel[x][y].b * 255));
         } 
    } 
    
    fprintf (stderr, "Rendering complete. Image saved as img.ppm\n"); 

    fclose (fp); 

    return 0; 
}

COLOR_T** malloc_array (int arraySizeX, int arraySizeY)
{
    int i; 
    COLOR_T **new_array; 

    new_array = (COLOR_T **) malloc (arraySizeX * sizeof(COLOR_T *));

    for ( i = 0; i < arraySizeX; ++i ) 
        new_array[i] = (COLOR_T *) malloc (arraySizeY * sizeof(COLOR_T)); 

    return new_array; 
}

void aspect_ratio (double *denominator, double *ratio)
{
    int numerator; 

    if (WIDTH > HEIGHT) 
    {
        *denominator = HEIGHT; 
        numerator    = WIDTH; 
    }
 
    /* If the height and with are equal, set denominator equal to whichever */
    else
    {
        *denominator = WIDTH;
        numerator    = HEIGHT; 
    }

   *ratio = ( (double) numerator / *denominator ) / 2; 

   /* Convert denominator to multiplication form, since it's more efficient (in main) */
   *denominator = 1.0 / *denominator; 
}
 
void define_objects (SCENE_T *scene)
{
    /* Background */
    scene->background = (COLOR_T) {0.3, 0.3, 0.5};    

    /* Lights */
    scene->lights[0].source = (VEC_PT) {5, 10, -2};
    scene->lights[0].intensity = (COLOR_T) {1, 1, 1};

    scene->lights[1].source = (VEC_PT) {-10, 4, -1};
    scene->lights[1].intensity = (COLOR_T) {0.3, 0.3, 0.3};
    
    /* Spheres */ 
    scene->objs[0].intersect = intersect_sphere; 
    scene->objs[0].geom.sphere.radius = 0.5;
    scene->objs[0].geom.sphere.center = (VEC_PT) {0.5, 0.8, 4.0}; 
    scene->objs[0].color = (COLOR_T) {0.8, 0, 0};
    scene->objs[0].color_ptr = get_color;   
    scene->objs[0].refl = 0.5; 

    scene->objs[1].intersect = intersect_sphere;
    scene->objs[1].geom.sphere.radius = 0.6;
    scene->objs[1].geom.sphere.center = (VEC_PT) {-0.5, 0.15, 4.2}; 
    scene->objs[1].color = (COLOR_T) {0, 0.8, 0};
    scene->objs[1].color_ptr = get_color; 
    scene->objs[1].refl = 0.3; 
   
    /* Plane */
    scene->objs[2].intersect = intersect_plane; 
    scene->objs[2].geom.plane.norm = (VEC_PT) {0, 1, 0};
    scene->objs[2].geom.plane.D = 0.9; 
    scene->objs[2].color = (COLOR_T) {1, 1, 1};
    scene->objs[2].color2 = (COLOR_T) {0, 0, 0};
    scene->objs[2].color_ptr = checkerboard; 
    scene->objs[2].refl = 0.4; 
   
    /* Box */
    scene->objs[3].intersect = intersect_box; 
    scene->objs[3].geom.box.b1[0] =  0.3; 
    scene->objs[3].geom.box.b1[1] = -0.6;   
    scene->objs[3].geom.box.b1[2] =  2.3;
    scene->objs[3].geom.box.b2[0] =  0.7; 
    scene->objs[3].geom.box.b2[1] = -0.2;   
    scene->objs[3].geom.box.b2[2] =  3.0;
    scene->objs[3].color = (COLOR_T) {0, 0, 0.7};
    scene->objs[3].color_ptr = get_color; 
    scene->objs[3].refl = 0.1; 
}     
 
COLOR_T trace (SCENE_T scene, RAY_T ray, int depth) 
{
    int i;
    bool hit; 
    int c_obj = -1; // c_obj is the closest object
    double t;
    double closest_t = 1000.0; 
    VEC_PT intersect_pt, norm; 
    VEC_PT closest_int_pt, closest_norm; 
    COLOR_T refl_color, regular_color, final_color = scene.background, black = {0, 0, 0};
    RAY_T refl_ray; 

    /* Restricts recursion to "depth" of 5, then returns black to symbolize photons' loss of energy */
    if (depth > 5)
        return black; 
   
    /* Loop that cycles through all objects. Function pointer guarantees that intersect_sphere
       and intersect_plane are called appropriately. */
    for ( i = 0; i < OBJ_NUM; ++i )  
    {
        
        
        if (scene.objs[i].intersect(scene.objs[i], ray, &intersect_pt, &norm, &t) == true)
        {    
            if (t > 0.01 && t < closest_t) 
            { 
                closest_t = t; 
                closest_norm = norm; 
                closest_int_pt = intersect_pt; 
                c_obj = i; 
            }
        }
    }
         
    if (c_obj >= 0) 
    {
        /* If object has reflectivity, compute reflective ray */      
        if (scene.objs[c_obj].refl > 0)
        { 
            refl_ray.or = closest_int_pt;        

	    refl_ray.dir.x = ray.dir.x - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.x);
	    refl_ray.dir.y = ray.dir.y - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.y);
	    refl_ray.dir.z = ray.dir.z - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.z);

            refl_ray.dir = vec_normalize (refl_ray.dir);
        
	    /* Call trace function with reflective ray */ 
	    refl_color = trace (scene, refl_ray, depth + 1); 
       }
		
       if (scene.objs[c_obj].refl < 1)    
           /* If object is not entirely reflective or not reflective, compute regular color */
           regular_color = do_lighting (scene, ray, closest_int_pt, closest_norm, c_obj);  
		
       /* After recursion process, mix colors */
       final_color.r = (scene.objs[c_obj].refl * refl_color.r) + ((1.0 - scene.objs[c_obj].refl) * regular_color.r); 
       final_color.g = (scene.objs[c_obj].refl * refl_color.g) + ((1.0 - scene.objs[c_obj].refl) * regular_color.g);
       final_color.b = (scene.objs[c_obj].refl * refl_color.b) + ((1.0 - scene.objs[c_obj].refl) * regular_color.b);
    }

    return final_color; 
}


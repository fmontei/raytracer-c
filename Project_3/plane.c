#include <stdbool.h>
#include "ray.h"

bool intersect_plane (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    *norm = obj.geom.plane.norm; 

    if (vec_dot(obj.geom.plane.norm, ray.dir) == 0)
        return false;

    else 
        *t = -1 * (vec_dot(obj.geom.plane.norm, ray.or) + obj.geom.plane.D) / 
                   vec_dot(obj.geom.plane.norm, ray.dir);  
 
    if  (*t < 0)
        return false; 
   
    int_pt->x = ray.or.x + ray.dir.x * *t;  
    int_pt->y = ray.or.y + ray.dir.y * *t;
    int_pt->z = ray.or.z + ray.dir.z * *t;  

    return true; 
}

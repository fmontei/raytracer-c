#include <stdbool.h>
#include <math.h>
#include "ray.h"

bool intersect_sphere (OBJ_T obj, RAY_T ray, VEC_PT *int_pt, VEC_PT *norm, double *t)
{
    double B, C, discriminant, t0, t1;   
      
    B = 2 * (ray.dir.x * (ray.or.x - obj.geom.sphere.center.x) + 
             ray.dir.y * (ray.or.y - obj.geom.sphere.center.y) + 
             ray.dir.z * (ray.or.z - obj.geom.sphere.center.z)); 
    C =     ((ray.or.x - obj.geom.sphere.center.x) * (ray.or.x - obj.geom.sphere.center.x) + 
             (ray.or.y - obj.geom.sphere.center.y) * (ray.or.y - obj.geom.sphere.center.y) +
             (ray.or.z - obj.geom.sphere.center.z) * (ray.or.z - obj.geom.sphere.center.z) -
             (obj.geom.sphere.radius) * (obj.geom.sphere.radius));  

    /* Calculate the discriminant, the part of the quadratic formula inside the square root */
    discriminant = B * B - (4 * C); 

    if (discriminant < 0)
        return false; 
    
    t0  = (-B - sqrt (discriminant)) / 2;  // A is omitted since it is equal to 1 
    t1  = (-B + sqrt (discriminant)) / 2;
      
    /* If the ray is moving in the negative direction, return false */
    if (t0 < 0 && t1 < 0) 
        return false;  

    if (t0 < t1 && t0 > 0)
        *t = t0;

    else
        *t = t1;    

    if (t1 < t0 && t1 > 0)
        *t = t1;

    else
        *t = t0;
    /* Calculate intersect points, points where ray and surface of sphere intersect */
    int_pt->x = ray.or.x + ray.dir.x * *t; 
    int_pt->y = ray.or.y + ray.dir.y * *t;
    int_pt->z = ray.or.z + ray.dir.z * *t; 

    /* Calculate the normal */
    norm->x = (int_pt->x - obj.geom.sphere.center.x) / obj.geom.sphere.radius; 
    norm->y = (int_pt->y - obj.geom.sphere.center.y) / obj.geom.sphere.radius;
    norm->z = (int_pt->z - obj.geom.sphere.center.z) / obj.geom.sphere.radius;  

    return true; 
} 



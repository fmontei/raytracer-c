#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "ray.h"
#include "sphere.h"
#include "vector.h"
#include "light.h"
#include "plane.h"


//////////////////////////////////////////
// Felipe Monteiro                      //
// cpsc102                              //
// Dr. Davis                            // 
// May 22, 2013                         //
// Project 2 -- Intermediate Ray Tracer //
//                                      //
// Ray tracer that uses more advanced   //
// coding techniques like function      //
// pointers, unions and multiple        //
// structures.This ray tracer can draw  //
// multiple spheres and planes. It      //
// calculates arbitrary aspect ratio,   //
// light attenuation and shadowing.     //
//////////////////////////////////////////


/* Ray-specific function prototypes */ 
void aspect_ratio (double *, double *); 

void print_ppm_header (FILE *);  

COLOR_T trace (SCENE_T, RAY_T, int);

void print_pixel (FILE *fp, COLOR_T color);

void define_objects (SCENE_T *);


int main (void)
{  
    FILE *fp;
    int x, y, depth = 0;
    double denominator, ratio; 
    RAY_T ray;
    COLOR_T color; 
    OBJ_T objs[OBJ_NUM]; 
    SCENE_T scene;

    fp = fopen ("img2.ppm", "w");  
    
    /* Function that calculates arbitrary aspect ratio */
    aspect_ratio (&denominator, &ratio);  

    fprintf (fp, "P6\n%d %d\n%d\n", WIDTH, HEIGHT, MAXV); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < HEIGHT; ++y ) 
    {
         for ( x = 0; x < WIDTH; ++x ) 
         {  
              ray.or = (VEC_PT) {0, 0, 0};

              // Convert ray.dir from 2D space to 3D space
              if (WIDTH > HEIGHT) 
              {
                  ray.dir.x = -ratio + ( (double) x * denominator ); 
                  ray.dir.y =  0.5   - ( (double) y * denominator ); 
                  ray.dir.z =  1.0;
              }
      
              else 
              {
                  ray.dir.x = -0.5   + ( (double) x * denominator ); 
                  ray.dir.y =  ratio - ( (double) y * denominator ); 
                  ray.dir.z =  1.0;
              }

              ray.dir = vec_normalize (ray.dir);      // Normalize ray direction

              define_objects (&scene);                // Define objects
              
              color = trace (scene, ray, depth); 
        
              print_pixel (fp, color); 
         } 
    } 
    
    fprintf (stderr, "Rendering complete. Image saved as img2.ppm\n"); 

    fclose (fp); 

    return 0; 
}

void aspect_ratio (double *denominator, double *ratio)
{
    int numerator; 

    if (WIDTH > HEIGHT) 
    {
        *denominator = HEIGHT; 
        numerator    = WIDTH; 
    }
 
    /* If the height and with are equal, set denominator equal to whichever */
    else
    {
        *denominator = WIDTH;
        numerator    = HEIGHT; 
    }

   *ratio = ( (double) numerator / *denominator ) / 2; 

   /* Convert denominator to multiplication form, since it's more efficient (in main) */
   *denominator = 1.0 / *denominator; 
}

void define_objects (SCENE_T *scene)
{
    /* Background */
    scene->background = (COLOR_T) {0.3, 0.3, 0.5};    

    /* Light source */
    scene->light.source = (VEC_PT) {5, 10, -2};
    scene->light.intensity = (COLOR_T) {1, 1, 1};
    
    /* Spheres */
    scene->objs[0].type = 0;
    scene->objs[0].intersect = intersect_sphere; 
    scene->objs[0].geom.sphere.radius = 0.5;
    scene->objs[0].geom.sphere.center = (VEC_PT) {0.5, 0.8, 4.0}; 
    scene->objs[0].color = (COLOR_T) {0.8, 0, 0};
    scene->objs[0].color_ptr = get_color; 
    scene->objs[0].refl = 0.8;

    scene->objs[1].type = 0;
    scene->objs[1].intersect = intersect_sphere;
    scene->objs[1].geom.sphere.radius = 0.6;
    scene->objs[1].geom.sphere.center = (VEC_PT) {-0.5, 0.15, 4.2}; 
    scene->objs[1].color = (COLOR_T) {0, 0.8, 0};
    scene->objs[1].color_ptr = get_color; 
    scene->objs[1].refl = 0;
   
    /* Plane */
    scene->objs[2].type = 1;
    scene->objs[2].intersect = intersect_plane; 
    scene->objs[2].geom.plane.norm = (VEC_PT) {0, 1, 0};
    scene->objs[2].geom.plane.D = 0.9; 
    scene->objs[2].color = (COLOR_T) {1, 1, 1};
    scene->objs[2].color2 = (COLOR_T) {0, 0, 0};
    scene->objs[2].color_ptr = checkerboard; 
    scene->objs[2].refl = 0;
}

COLOR_T trace (SCENE_T scene, RAY_T ray, int depth) 
{
    int i, hit; 
    int c_obj = -1; // c_obj is the closest object
    double t;
    double closest_t = 1000.0; 
    VEC_PT intersect_pt, norm; 
    VEC_PT closest_int_pt, closest_norm; 

    RAY_T refl_ray;
    COLOR_T refl_color, regular_color, final_color = scene.background; 
    COLOR_T black = (COLOR_T) {0, 0, 0};

    if (depth > 5)
        return black; 
   
    /* Loop that cycles through all objects. Function pointer guarantees that intersect_sphere
       and intersect_plane are called appropriately. */
    for ( i = 0; i < OBJ_NUM; ++i ) 
    {
        hit = scene.objs[i].intersect(scene.objs[i], ray, &intersect_pt, &norm, &t); 
 
        if (hit == 1) 
        {
            if (t < closest_t) 
            { 
                closest_t = t; 
                closest_norm = norm; 
                closest_int_pt = intersect_pt; 
                c_obj = i; 
            } 
      
            /* END FOR LOOP HERE FOR LESS DISTORTION */
 
            if (scene.objs[c_obj].refl > 0) 
            {
                /* compute reflected ray */
                refl_ray.or = closest_int_pt; 

                refl_ray.dir.x = ray.dir.x - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.x);
                refl_ray.dir.y = ray.dir.y - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.y);
                refl_ray.dir.z = ray.dir.z - (2 * vec_dot(closest_norm, ray.dir) * closest_norm.z);

                refl_ray.dir = vec_normalize (refl_ray.dir); 
          
                /* call trace recursively with new reflected ray */
                refl_color = trace (scene, refl_ray, depth + 1); 

                if (refl_color.r > 1) refl_color.r = 1; 
                if (refl_color.g > 1) refl_color.g = 1; 
                if (refl_color.b > 1) refl_color.b = 1;
                if (refl_color.r < 0) refl_color.r = 0; 
                if (refl_color.g < 0) refl_color.g = 0; 
                if (refl_color.b < 0) refl_color.b = 0;
                  
            }

            if (scene.objs[c_obj].refl < 1)
            {
                regular_color = do_lighting (scene, ray, closest_int_pt, closest_norm, c_obj); 
            }      
        }
    }   
             
    if (scene.objs[c_obj].refl == 0) 
    {
        final_color = regular_color;    
    }

    /* mix colors */
    if (scene.objs[c_obj].refl > 0)
    {
        final_color.r = (scene.objs[c_obj].refl * refl_color.r) + ((1.0 - scene.objs[c_obj].refl) * regular_color.r); 
        final_color.g = (scene.objs[c_obj].refl * refl_color.g) + ((1.0 - scene.objs[c_obj].refl) * regular_color.g); 
        final_color.b = (scene.objs[c_obj].refl * refl_color.b) + ((1.0 - scene.objs[c_obj].refl) * regular_color.b);
    }
 
    return final_color; 
}

void print_pixel (FILE *fp, COLOR_T color)
{
    /* If statements handle "overflow" */
    if (color.r > 1) color.r = 1; 
    if (color.g > 1) color.g = 1; 
    if (color.b > 1) color.b = 1; 
 
    fprintf (fp, "%c%c%c", (unsigned char) (color.r * 255), 
                           (unsigned char) (color.g * 255),
                           (unsigned char) (color.b * 255));
}

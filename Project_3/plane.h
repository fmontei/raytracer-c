#ifndef _PLANE_H
#define _PLANE_H

#include <stdbool.h>
#include "ray.h"

bool intersect_plane (OBJ_T, RAY_T, VEC_PT *, VEC_PT *, double *);

#endif

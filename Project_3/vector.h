#ifndef _VECTOR_H
#define _VECTOR_H 

typedef struct {
  double x; 
  double y; 
  double z; 
}  VEC_PT;

/* Vector-specific function prototyes */
VEC_PT vec_normalize (VEC_PT);

double vec_dot (VEC_PT, VEC_PT);

VEC_PT vec_subtract (VEC_PT, VEC_PT);

double vec_len (VEC_PT);

VEC_PT reverse_ray (VEC_PT);

#endif

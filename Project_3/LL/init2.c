#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include "ray.h"
#include "box.h"
#include "sphere.h"
#include "plane.h"
#include "light.h"

void skip_white_space (FILE *fp) 
{
    char ch; 

    ch = fgetc (fp); 

    while (isspace (ch))
        ch = fgetc (fp);       
 
    ungetc (ch, fp); 
}

void init (SCENE_T *scene, IMG_T *image)
{
    char line[100];
    char ch;
    int i = 0, j = 0; 
    FILE *fp; 

    OBJ_T *obj_node = NULL;
    LIGHT_T *light_node = NULL;

    image->num_obj = image->num_light = 0; 

    if ((fp = fopen ("scene.txt", "r")) == NULL)
        fprintf (stderr, "Error: text file cannot be opened\n"); 
         
    /* Reading in character by character enables skipping white space */
    while (ch != EOF)
    {
        skip_white_space (fp); 
        ch = fgetc(fp); 

        if (isalpha(ch))
        {
            ungetc (ch, fp);
            fscanf (fp, "%s", line);  
            
            if (strcmp (line, "width") == 0) 
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->width);  
            }       

            if (strcmp (line, "height") == 0)
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->height);  
            } 

 	    if (strcmp (line, "background") == 0)
	    {
		skip_white_space (fp); 
		fscanf (fp, "%lf %lf %lf", &scene->background.r, &scene->background.g, 
			&scene->background.b);
	    }

            if (strcmp (line, "sphere") == 0)
            {
		obj_node = (OBJ_T *) malloc (sizeof(OBJ_T)); 

                obj_node->intersect = intersect_sphere;
                obj_node->color_ptr = get_color;

                /* sphere center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &obj_node->geom.sphere.center.x, 
                        &obj_node->geom.sphere.center.y, &obj_node->geom.sphere.center.z);
     
                /* sphere radius */
    		skip_white_space (fp);
                fscanf (fp, "%lf", &obj_node->geom.sphere.radius); 
 
                /* sphere color */
 		skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &obj_node->color.r,
                        &obj_node->color.g, &obj_node->color.b);

                /* sphere reflectivity */
		skip_white_space (fp);
                fscanf (fp, "%lf", &obj_node->refl);
                
                fprintf (stderr, "\nObject no. = %d\nSphere center = %.1f, %.1f, %.1f\n", i, 
                         obj_node->geom.sphere.center.x, obj_node->geom.sphere.center.y, 
                         obj_node->geom.sphere.center.z); 
                fprintf (stderr, "Sphere radius = %.1f\n", obj_node->geom.sphere.radius); 
                fprintf (stderr, "Sphere color = %.1f, %.1f, %.1f\n", obj_node->color.r,
                         obj_node->color.g, obj_node->color.b);
                fprintf (stderr, "Sphere reflectivity = %.1f\n", obj_node->refl); 

 		obj_node->next = scene->objs;	//Link node in front of head
		scene->objs = obj_node;		//Set head to new node

                ++i; 
	        ++image->num_obj;
            } 

            if (strcmp (line, "plane") == 0)
            {
		obj_node = (OBJ_T *) malloc (sizeof(OBJ_T));

                obj_node->intersect = intersect_plane; 
                obj_node->color_ptr = checkerboard;

                /* plane normal */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &obj_node->geom.plane.norm.x, 
                        &obj_node->geom.plane.norm.y, &obj_node->geom.plane.norm.z);
 
                /* plane D */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &obj_node->geom.plane.D);

                /* plane color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &obj_node->color.r, 
                        &obj_node->color.g, &obj_node->color.b);

                /* plane color 2 */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &obj_node->color2.r, 
                        &obj_node->color2.g, &obj_node->color2.b);

                /* plane reflectivity */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &obj_node->refl); 
                
                fprintf (stderr, "\nObject no. %d\nPlane norm = %.1f, %.1f, %.1f\n", i, 
                         obj_node->geom.plane.norm.x, obj_node->geom.plane.norm.y, 
	 		 obj_node->geom.plane.norm.z);
                fprintf (stderr, "Plane color = %.1f, %.1f, %.1f\n", obj_node->color.r, 
                        obj_node->color.g, obj_node->color.b);
                fprintf (stderr, "Plane color2 = %.1f, %.1f, %.1f\n", obj_node->color2.r, 
                        obj_node->color2.g, obj_node->color2.b);
                fprintf (stderr, "Plane reflectivity = %.1f\n", obj_node->refl);
             
		obj_node->next = scene->objs;
 		scene->objs = obj_node; 

                ++i;
	        ++image->num_obj;
            }

            if (strcmp (line, "light") == 0)
            { 
		light_node = (LIGHT_T *) malloc (sizeof(LIGHT_T));

                /* light center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &light_node->source.x, &light_node->source.y, 
 		        &light_node->source.z); 

                /* light intensity */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &light_node->intensity.r, &light_node->intensity.g,
		        &light_node->intensity.b); 

		fprintf (stderr, "\nLight no. = %d\nLight source = %.1f, %.1f, %.1f\n", j,
                         light_node->source.x, light_node->source.y, light_node->source.z); 
  		fprintf (stderr, "Light intensity = %.1f, %.1f, %.1f\n", 
       			 light_node->intensity.r, light_node->intensity.g, light_node->intensity.b);

		light_node->next = scene->lights;
 		scene->lights = light_node; 

      		++j;    
	        ++image->num_light;
           }

           if (strcmp (line, "box") == 0)
           {
	       obj_node = (OBJ_T *) malloc (sizeof(OBJ_T));

               obj_node->intersect = intersect_box;
               obj_node->color_ptr = get_color;

               /* box lower-left */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &obj_node->geom.box.b1[0], 
                       &obj_node->geom.box.b1[1], &obj_node->geom.box.b1[2]);

               /* box upper-right */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &obj_node->geom.box.b2[0], 
                       &obj_node->geom.box.b2[1], &obj_node->geom.box.b2[2]); 

                /* box color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &obj_node->color.r, 
                        &obj_node->color.g, &obj_node->color.b);

 		/* box reflectivity */
                skip_white_space (fp);
   		fscanf (fp, "%lf", &obj_node->refl); 

 		fprintf (stderr, "\nObject no. = %d\n Box lower-left = %.1f, %.1f, %.1f\n", i, 
                         obj_node->geom.box.b1[0], obj_node->geom.box.b1[1], obj_node->geom.box.b1[2]); 
                fprintf (stderr, "Box upper-right = %.1f, %.1f, %.1f\n",
                         obj_node->geom.box.b2[0], obj_node->geom.box.b2[1], obj_node->geom.box.b2[2]); 
                fprintf (stderr, "Box color = %.1f, %.1f, %.1f\n", obj_node->color.r,
                         obj_node->color.g, obj_node->color.b);
                fprintf (stderr, "Box reflectivity = %.1f\n", obj_node->refl);

		obj_node->next = scene->objs;
 		scene->objs = obj_node; 

 		++i; 
                ++image->num_obj;
	    }
              
        }  
    }

    fclose (fp); 
}
#if 0
int main (void)
{
    SCENE_T scene;
    IMG_T image;

    int ct = 0;

    OBJ_T *curr;
    LIGHT_T *curr2;

    scene.objs = NULL;
    scene.lights = NULL;

    init (&scene, &image);

    fprintf (stderr, "\nIN MAIN\n");

    for ( curr = scene.objs; curr != NULL; curr = curr->next )
    {
        fprintf (stderr, "Object %d\n", ct);
        ++ct; 
    }

    ct = 0; 

    for ( curr2 = scene.lights; curr2 != NULL; curr2 = curr2->next )
    {
        fprintf (stderr, "Light %d\n", ct);
        ++ct; 
    }

    fprintf (stderr, "number of objects = %d, lights = %d\n", image.num_obj, image.num_light); 

    return 0; 
}
#endif


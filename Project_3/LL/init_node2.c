#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "ray.h"
#include "box.h"
#include "sphere.h"
#include "plane.h"
#include "light.h"

typedef struct node {
  SCENE_T scene;
  struct node *next;
}  NODE_T; 

void printList(NODE_T* current)
{
	//first see if current == NULL, then print "empty list"
	
	//then keep printing the data and iterating through the nodes

        int i = 0;

        if (current != NULL) {

	   for ( ; current != NULL; current = current->next ) 
           {
              printf ("Entry no. %d = %d\n", i, i);
              fprintf (stderr, "sphere %d center = {%.1f, %.1f, %.1f}\n", i, 
                       current->scene.objs[i].geom.sphere.center.x, current->scene.objs[i].geom.sphere.center.y, 
                       current->scene.objs[i].geom.sphere.center.z); 
             ++i;
           }
        }

        else
           printf ("Empty\n");  
}

NODE_T* addToFront (NODE_T *temp, SCENE_T *scene)
{
	//add a new node to the very front of the list
	//initialize the node, and point to the current head
	NODE_T *new; 

        if (temp == NULL) {
            return; fprintf (stderr, "NULL\n"); 
        }

        if ((new = (NODE_T *)malloc(sizeof(NODE_T))) == NULL)
            exit (1); 

        new->scene = *scene;
        new->next = temp; 
	//returns new head
        return new; 
}

void skip_white_space (FILE *fp) 
{
    char ch; 

    ch = fgetc (fp); 

    while (isspace (ch))
        ch = fgetc (fp);       
 
    ungetc (ch, fp); 
}

NODE_T* init (SCENE_T *scene, IMG_T *image, NODE_T *head)
{
    char line[100];
    char ch;
     
    int i = 0, j = 0; 

    FILE *fp;  

    if ((fp = fopen ("scene.txt", "r")) == NULL)
        fprintf (stderr, "Error: text file cannot be opened\n"); 

    while (fscanf (fp, "%s", line) != EOF)
    {
        if (strcmp (line, "sphere") == 0) ++image->num_obj;
        if (strcmp (line, "plane")  == 0) ++image->num_obj;
        if (strcmp (line, "box")    == 0) ++image->num_obj;
	if (strcmp (line, "light")  == 0) ++image->num_light;
    }

    scene->lights = (LIGHT_T *) malloc (image->num_light * sizeof(LIGHT_T));
    scene->objs = (OBJ_T *) malloc (image->num_obj * sizeof(OBJ_T));
    
    rewind (fp);
     
    NODE_T *new_node, *new_node2, *new_node3, *new_node4, *curr = head; 

    while (ch != EOF)
    {
        skip_white_space (fp); 
        ch = fgetc(fp); 

        if (isalpha(ch))
        {
            ungetc (ch, fp);
            fscanf (fp, "%s", line);  
            
            if (strcmp (line, "width") == 0) 
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->width);  
            }       

            if (strcmp (line, "height") == 0)
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->height);  
            } 

            if (strcmp (line, "sphere") == 0)
            {
		if ((new_node = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
		    exit (1);

                //scene = (SCENE_T *) malloc ((i + 1) * sizeof(SCENE_T));
                scene->objs[i].intersect = intersect_sphere;
                scene->objs[i].color_ptr = get_color;

                /* sphere center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.sphere.center.x, 
                        &scene->objs[i].geom.sphere.center.y, &scene->objs[i].geom.sphere.center.z);
     
                /* sphere radius */
    		skip_white_space (fp);
                fscanf (fp, "%lf", &scene->objs[i].geom.sphere.radius); 
 
                /* sphere color */
 		skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r,
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

                /* sphere reflectivity */
		skip_white_space (fp);
                fscanf (fp, "%lf", &scene->objs[i].refl);
                
                fprintf (stderr, "\nsphere %d center = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.sphere.center.x, scene->objs[i].geom.sphere.center.y, 
                         scene->objs[i].geom.sphere.center.z); 
                fprintf (stderr, "sphere %d radius = %.1f\n", i, scene->objs[i].geom.sphere.radius); 
                fprintf (stderr, "sphere %d color = %.1f, %.1f, %.1f\n", i, scene->objs[i].color.r,
                         scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "sphere %d refl = %.1f\n", i, scene->objs[i].refl);  
	
                new_node->scene = *scene; 
                new_node->next = curr; 
	        curr = new_node; 
 
                ++i; 
            } 

            if (strcmp (line, "plane") == 0)
            {
                if ((new_node2 = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
		    exit (1);

                scene->objs[i].intersect = intersect_plane; 
                scene->objs[i].color_ptr = checkerboard;

                /* plane normal */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.plane.norm.x, 
                        &scene->objs[i].geom.plane.norm.y, &scene->objs[i].geom.plane.norm.z);
 
                /* plane D */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].geom.plane.D);

                /* plane color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

                /* plane color 2 */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color2.r, 
                        &scene->objs[i].color2.g, &scene->objs[i].color2.b);

                /* plane reflectivity */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].refl); 
                
                fprintf (stderr, "\nplane %d norm = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].geom.plane.norm.x, 
                         scene->objs[i].geom.plane.norm.y, scene->objs[i].geom.plane.norm.z);
                fprintf (stderr, "plane %d color = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color.r, 
                        scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "plane %d color2 = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color2.r, 
                        scene->objs[i].color2.g, scene->objs[i].color2.b);
                fprintf (stderr, "plane %d reflectivity = %.1f\n", i, scene->objs[i].refl);

		new_node2->scene = *scene; 
                new_node2->next = curr; 
	        curr = new_node2; 

                ++i;
            }

	   if (strcmp (line, "box") == 0)
           {
               if ((new_node3 = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
		    exit (1);

               scene->objs[i].intersect = intersect_box;
               scene->objs[i].color_ptr = get_color;

               /* box lower-left */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b1[0], 
                       &scene->objs[i].geom.box.b1[1], &scene->objs[i].geom.box.b1[2]);

               /* box upper-right */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b2[0], 
                       &scene->objs[i].geom.box.b2[1], &scene->objs[i].geom.box.b2[2]); 

                /* box color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

 		/* box reflectivity */
                skip_white_space (fp);
   		fscanf (fp, "%lf", &scene->objs[i].refl); 

 		fprintf (stderr, "\nbox %d ll = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b1[0], scene->objs[i].geom.box.b1[1], scene->objs[i].geom.box.b1[2]); 
                fprintf (stderr, "box %d ur = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b2[0], scene->objs[i].geom.box.b2[1], scene->objs[i].geom.box.b2[2]); 
                fprintf (stderr, "box %d color = %.1f, %.1f, %.1f\n", i, scene->objs[i].color.r,
                         scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "box %d refl = %.1f\n", i, scene->objs[i].refl);

		new_node3->scene = *scene; 
                new_node3->next = curr; 
	        curr = new_node3; 

 		++i; 
	    }
            #if 0
            if (strcmp (line, "light") == 0)
            { 
		 
                //if ((new_node4 = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
		//    exit (1);
		
		SCENE_T scene2;
		scene2.lights = (LIGHT_T *) malloc (2 *sizeof(LIGHT_T)); 
                /* light center */
                skip_white_space (fp); 

                fscanf (fp, "%lf %lf %lf", &scene2.lights[j].source.x, &scene2.lights[j].source.y, 
 		        &scene2.lights[j].source.z); 

                /* light intensity */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene2.lights[j].intensity.r, &scene2.lights[j].intensity.g,
		        &scene2.lights[j].intensity.b); 

		fprintf (stderr, "\nlight %d source = %.1f, %.1f, %.1f\n", j,
                         scene2.lights[j].source.x, scene->lights[j].source.y, scene2.lights[j].source.z); 
  		fprintf (stderr, "light %d intensity = %.1f, %.1f, %.1f\n", j, 
       			 scene2.lights[j].intensity.r, scene2.lights[j].intensity.g, scene2.lights[j].intensity.b);

		//new_node4->scene = *scene; 
                //new_node4->next = curr; 
	        //curr = new_node4;

      		++j;    
	       
           }
           #endif   
        }  

    }
	
    scene->background = (COLOR_T) {0.2, 0, 0.3};
     
    fprintf (stderr, "\nnumber of objects = %d, number of lights = %d\n\n", i, j); 

    fclose (fp); 
	
    return new_node3; 
}


int main (void)
{
    SCENE_T scene;
    IMG_T image;

    NODE_T *head = NULL; 

    head = init (&scene, &image, head);

    fprintf (stderr, "\nIN MAIN\n\n");

    printList (head); 


    return 0;
}

#ifndef _LIGHT_H
#define _LIGHT_H

#include "ray.h"

COLOR_T do_lighting (IMG_T, SCENE_T, RAY_T, VEC_PT, VEC_PT, OBJ_T *);

COLOR_T refl_specular (IMG_T, SCENE_T, RAY_T, VEC_PT, VEC_PT, COLOR_T);

COLOR_T checkerboard (OBJ_T *, VEC_PT);

COLOR_T get_color (OBJ_T *, VEC_PT);

#endif

#include "ray.h"
#include <malloc.h>
#include <stdio.h>

int main (void)
{
    SCENE_T scene; 

    LIGHT_T *new_light;
    new_light = (LIGHT_T *) malloc (sizeof(LIGHT_T));

    new_light->source = (VEC_PT) {0, 1, 0};
    new_light->intensity = (COLOR_T) {0.2, 0.2, 0.2}; 
    
    scene.lights = new_light; 
 
    printf ("%.1f %.1f %.1f\n", scene.lights->source.x, scene.lights->source.y, scene.lights->source.z);  


    return 0;
}


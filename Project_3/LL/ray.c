#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <malloc.h> 
#include "ray.h" 
#include "sphere.h"
#include "vector.h" 
#include "light.h"
#include "plane.h"
#include "box.h"  
#include "init.h"


//////////////////////////////////////////
// Felipe Monteiro                      //
// cpsc102                              //
// Dr. Davis                            // 
// May 22, 2013                         //
// Project 2 -- Intermediate Ray Tracer //
//                                      //
// Ray tracer that uses more advanced   //
// coding techniques like function      //
// pointers, unions and multiple        //
// structures.This ray tracer can draw  //
// multiple spheres and planes. It      //
// calculates arbitrary aspect ratio,   //
// light attenuation and shadowing.     //
//////////////////////////////////////////


/* Ray-specific function prototypes */ 
void aspect_ratio (IMG_T, double *, double *); 

void print_ppm_header (FILE *);  

COLOR_T trace (IMG_T, SCENE_T, RAY_T, int);

void print_pixel (FILE *fp, COLOR_T **);

void define_objects (SCENE_T *);

COLOR_T** malloc_array (int, int); 
  
  
int main (void)
{  
    FILE *fp;
    int x, y;
    int depth = 0;  
    double denominator, ratio; 
    RAY_T ray; 
    SCENE_T scene; 
    IMG_T image;    

    fp = fopen ("img.ppm", "w"); 

    init (&scene, &image);
  
    //define_objects (&scene);                
     
    aspect_ratio (image, &denominator, &ratio);  

    COLOR_T **pixel = malloc_array (image.width, image.height);
    
    fprintf (fp, "P6\n%d %d\n%d\n", image.width, image.height, 255); 
    fprintf (stderr, "Rendering image...\n"); 

    for ( y = 0; y < image.height; ++y ) 
    {
         for ( x = 0; x < image.width; ++x ) 
         {  
              ray.or = (VEC_PT) {0, 0, 0};

              // Convert ray.dir from 2D space to 3D space
              if (image.width > image.height) 
              {
                  ray.dir.x = -ratio + ((double) x * denominator); 
                  ray.dir.y =  0.5   - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }
      
              else 
              {
                  ray.dir.x = -0.5   + ((double) x * denominator); 
                  ray.dir.y =  ratio - ((double) y * denominator); 
                  ray.dir.z =  1.0;
              }

              ray.dir = vec_normalize (ray.dir);      // Normalize ray direction
              
              pixel[x][y] = trace (image, scene, ray, depth); 
        
              if (pixel[x][y].r > 1) pixel[x][y].r = 1; 
    	      if (pixel[x][y].g > 1) pixel[x][y].g = 1; 
              if (pixel[x][y].b > 1) pixel[x][y].b = 1; 

              fprintf (fp, "%c%c%c", (unsigned char) (pixel[x][y].r * 255), 
              		             (unsigned char) (pixel[x][y].g * 255),
              		             (unsigned char) (pixel[x][y].b * 255));
         } 
    } 
    
    fprintf (stderr, "Rendering complete. Image saved as img.ppm\n"); 

    free (pixel); 
    free (scene.objs);
    free (scene.lights); 

    fclose (fp);

    return 0; 
}

COLOR_T** malloc_array (int arraySizeX, int arraySizeY)
{
    int i; 
    COLOR_T **new_array; 

    new_array = (COLOR_T **) malloc (arraySizeX * sizeof(COLOR_T *));

    for ( i = 0; i < arraySizeX; ++i ) 
        new_array[i] = (COLOR_T *) malloc (arraySizeY * sizeof(COLOR_T)); 

    return new_array; 
}

void aspect_ratio (IMG_T image, double *denominator, double *ratio)
{
    int numerator; 

    if (image.width > image.height) 
    {
        *denominator = image.height; 
        numerator    = image.width; 
    }
 
    /* If the height and with are equal, set denominator equal to whichever */
    else
    {
        *denominator = image.width;
        numerator    = image.height; 
    }

   *ratio = ((double) numerator / *denominator) / 2; 

   /* Convert denominator to multiplication form, since it's more efficient (in main) */
   *denominator = 1.0 / *denominator; 
}
 
void define_objects (SCENE_T *scene)
{
    /* Background */
    scene->background = (COLOR_T) {0.3, 0.3, 0.5};    

    /* Lights */
    scene->lights[0].source = (VEC_PT) {2.5, 5.0, -1.0};
    scene->lights[0].intensity = (COLOR_T) {0.6, 0.6, 0.6};

    scene->lights[1].source = (VEC_PT) {0.0, 4.0, -1.0};
    scene->lights[1].intensity = (COLOR_T) {0.7, 0.7, 0.7};
    
    /* Spheres */ 
    scene->objs[0].intersect = intersect_sphere; 
    scene->objs[0].geom.sphere.radius = 0.5;
    scene->objs[0].geom.sphere.center = (VEC_PT) {0.5, 0.8, 4.0}; 
    scene->objs[0].color = (COLOR_T) {0.8, 0.0, 0.0};
    scene->objs[0].color_ptr = get_color;   
    scene->objs[0].refl = 0.5; 

    scene->objs[1].intersect = intersect_sphere;
    scene->objs[1].geom.sphere.radius = 0.6;
    scene->objs[1].geom.sphere.center = (VEC_PT) {-0.5, 0.15, 4.2}; 
    scene->objs[1].color = (COLOR_T) {0, 0.8, 0};
    scene->objs[1].color_ptr = get_color; 
    scene->objs[1].refl = 0.3; 
   
    /* Plane */
    scene->objs[2].intersect = intersect_plane; 
    scene->objs[2].geom.plane.norm = (VEC_PT) {0, 1, 0};
    scene->objs[2].geom.plane.D = 0.9; 
    scene->objs[2].color = (COLOR_T) {1, 1, 1};
    scene->objs[2].color2 = (COLOR_T) {0, 0, 0};
    scene->objs[2].color_ptr = checkerboard; 
    scene->objs[2].refl = 0.4; 
   
    /* Box */
    scene->objs[3].intersect = intersect_box; 
    scene->objs[3].geom.box.b1[0] =  0.3; 
    scene->objs[3].geom.box.b1[1] = -0.6;   
    scene->objs[3].geom.box.b1[2] =  2.3;
    scene->objs[3].geom.box.b2[0] =  0.7; 
    scene->objs[3].geom.box.b2[1] = -0.2;   
    scene->objs[3].geom.box.b2[2] =  3.0;
    scene->objs[3].color = (COLOR_T) {0, 0, 0.7};
    scene->objs[3].color_ptr = get_color; 
    scene->objs[3].refl = 0;  
}     
 
COLOR_T trace (IMG_T image, SCENE_T scene, RAY_T ray, int depth) 
{
    int i;
    int c_obj = -1; // c_obj is the closest object
    double t;
    double closest_t = 1000.0; 
    VEC_PT intersect_pt, norm; 
    VEC_PT closest_int_pt, closest_norm; 
    COLOR_T refl_color, regular_color, final_color = scene.background, black = {0, 0, 0};
    RAY_T refl_ray; 

    /* Restricts recursion to "depth" of 5, then returns black to symbolize photons' loss of energy */
    if (depth > 5)
        return black; 
   
    /* Loop that cycles through all objects. Function pointer guarantees that intersect_sphere
       and intersect_plane are called appropriately. */
    for ( i = 0; i < image.num_obj; ++i )  
    {
        if (scene.objs[i].intersect(scene.objs[i], ray, &intersect_pt, &norm, &t) == true)
        {    
            if (t > 0.01 && t < closest_t) 
            { 
                closest_t = t; 
                closest_norm = norm; 
                closest_int_pt = intersect_pt; 
                c_obj = i; 
            }
        }
    }
         
    if (c_obj >= 0) 
    {
        /* If object has reflectivity, compute reflective ray */      
        if (scene.objs[c_obj].refl >= 0)
        { 
            refl_ray.or = closest_int_pt;        

	    refl_ray.dir.x = ray.dir.x - (2.0 * vec_dot(closest_norm, ray.dir) * closest_norm.x);
	    refl_ray.dir.y = ray.dir.y - (2.0 * vec_dot(closest_norm, ray.dir) * closest_norm.y);
	    refl_ray.dir.z = ray.dir.z - (2.0 * vec_dot(closest_norm, ray.dir) * closest_norm.z);

            refl_ray.dir = vec_normalize (refl_ray.dir);
        
	    /* Call trace function with reflective ray */  
	    refl_color = trace (image, scene, refl_ray, depth + 1);   
       }
		
       if (scene.objs[c_obj].refl <= 1)    
           /* If object is not entirely reflective or not reflective, compute regular color */
           regular_color = do_lighting (image, scene, ray, closest_int_pt, closest_norm, c_obj);  

       /* After recursion process, mix colors */
       final_color.r = (scene.objs[c_obj].refl * refl_color.r) + ((1.0 - scene.objs[c_obj].refl) * regular_color.r); 
       final_color.g = (scene.objs[c_obj].refl * refl_color.g) + ((1.0 - scene.objs[c_obj].refl) * regular_color.g);
       final_color.b = (scene.objs[c_obj].refl * refl_color.b) + ((1.0 - scene.objs[c_obj].refl) * regular_color.b);

       if (scene.objs[c_obj].refl > 0 && scene.objs[c_obj].intersect == intersect_sphere)
           final_color = refl_specular (image, scene, ray, closest_int_pt, closest_norm, final_color);    
    }

    return final_color;
} 


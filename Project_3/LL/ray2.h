#ifndef _RAY_H
#define _RAY_H

#include <stdbool.h>
#include "vector.h"

typedef struct {
  double r;
  double g;
  double b; 
}  COLOR_T; 

typedef struct {
  VEC_PT or;
  VEC_PT dir;
}  RAY_T;

/* Additional type created */
typedef struct LIGHT_TYPE {
  VEC_PT source;
  COLOR_T intensity;
  struct LIGHT_TYPE *next; 
}  LIGHT_T; 

typedef struct { 
  VEC_PT center;
  double radius;
}  SPHERE_T; 

typedef struct {
  VEC_PT norm;
  double D;
}  PLANE_T; 

typedef struct {
  double b1[3]; 
  double b2[3]; 
}  BOX_T; 

/* (*intersect) and (*color) are function pointers: the former points to
   either interspect_plane or intersect_sphere; the latter points to 
   either get_color (for spheres) and checkerboard (for planes) */ 
typedef struct OBJ_TYPE {
  double refl; 
  COLOR_T color, color2; 
  bool (*intersect) (struct OBJ_TYPE *, RAY_T, VEC_PT *, VEC_PT *, double *); 
  COLOR_T (*color_ptr) (struct OBJ_TYPE, VEC_PT);  // struct OBJ_TYPE is a structure tag

   union {
    SPHERE_T sphere; 
    PLANE_T  plane;
    BOX_T    box; 
  }  geom;
   
  struct OBJ_TYPE *next; 
}  OBJ_T; 

typedef struct {
  LIGHT_T *lights;
  OBJ_T *objs;
  COLOR_T background;  
}  SCENE_T;

typedef struct {
  int width;
  int height;
  int num_obj;
  int num_light;
}  IMG_T; 
  
#endif

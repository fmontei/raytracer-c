#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include "ray.h"
#include "box.h"
#include "sphere.h"
#include "plane.h"
#include "light.h"

void skip_white_space (FILE *fp) 
{
    char ch; 

    ch = fgetc (fp); 

    while (isspace (ch))
        ch = fgetc (fp);       
 
    ungetc (ch, fp); 
}

void init (SCENE_T *scene, IMG_T *image)
{
    char line[100];
    char ch;
    int i = 0, j = 0; 
    FILE *fp; 

    image->num_obj = image->num_light = 0; 

    if ((fp = fopen ("scene.txt", "r")) == NULL)
        fprintf (stderr, "Error: text file cannot be opened\n"); 

    while (fscanf (fp, "%s", line) != EOF)
    {
        if (strcmp (line, "sphere") == 0) ++image->num_obj;
        if (strcmp (line, "plane")  == 0) ++image->num_obj;
        if (strcmp (line, "box")    == 0) ++image->num_obj;
	if (strcmp (line, "light")  == 0) ++image->num_light;
    }

    scene->lights = (LIGHT_T *) malloc (image->num_light * sizeof(LIGHT_T));
    scene->objs = (OBJ_T *) malloc (image->num_obj * sizeof(OBJ_T));
    
    rewind (fp);
         
    /* Reading in character by character enables skipping white space */
    while (ch != EOF)
    {
        skip_white_space (fp); 
        ch = fgetc(fp); 

        if (isalpha(ch))
        {
            ungetc (ch, fp);
            fscanf (fp, "%s", line);  
            
            if (strcmp (line, "width") == 0) 
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->width);  
            }       

            if (strcmp (line, "height") == 0)
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->height);  
            } 

 	    if (strcmp (line, "background") == 0)
	    {
		skip_white_space (fp); 
		fscanf (fp, "%lf %lf %lf", &scene->background.r, &scene->background.g, 
			&scene->background.b);

	    }

            if (strcmp (line, "sphere") == 0)
            {
                scene->objs[i].intersect = intersect_sphere;
                scene->objs[i].color_ptr = get_color;

                /* sphere center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.sphere.center.x, 
                        &scene->objs[i].geom.sphere.center.y, &scene->objs[i].geom.sphere.center.z);
     
                /* sphere radius */
    		skip_white_space (fp);
                fscanf (fp, "%lf", &scene->objs[i].geom.sphere.radius); 
 
                /* sphere color */
 		skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r,
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

                /* sphere reflectivity */
		skip_white_space (fp);
                fscanf (fp, "%lf", &scene->objs[i].refl);
                
                fprintf (stderr, "\nsphere %d center = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.sphere.center.x, scene->objs[i].geom.sphere.center.y, 
                         scene->objs[i].geom.sphere.center.z); 
                fprintf (stderr, "sphere %d radius = %.1f\n", i, scene->objs[i].geom.sphere.radius); 
                fprintf (stderr, "sphere %d color = %.1f, %.1f, %.1f\n", i, scene->objs[i].color.r,
                         scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "sphere %d refl = %.1f\n", i, scene->objs[i].refl); 

                ++i; 
            } 

            if (strcmp (line, "plane") == 0)
            {
                scene->objs[i].intersect = intersect_plane; 
                scene->objs[i].color_ptr = checkerboard;

                /* plane normal */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.plane.norm.x, 
                        &scene->objs[i].geom.plane.norm.y, &scene->objs[i].geom.plane.norm.z);
 
                /* plane D */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].geom.plane.D);

                /* plane color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

                /* plane color 2 */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color2.r, 
                        &scene->objs[i].color2.g, &scene->objs[i].color2.b);

                /* plane reflectivity */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].refl); 
                
                fprintf (stderr, "\nplane %d norm = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].geom.plane.norm.x, 
                         scene->objs[i].geom.plane.norm.y, scene->objs[i].geom.plane.norm.z);
                fprintf (stderr, "plane %d color = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color.r, 
                        scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "plane %d color2 = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color2.r, 
                        scene->objs[i].color2.g, scene->objs[i].color2.b);
                fprintf (stderr, "plane %d reflectivity = %.1f\n", i, scene->objs[i].refl);
             
                ++i;
            }

            if (strcmp (line, "light") == 0)
            { 
                /* light center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->lights[j].source.x, &scene->lights[j].source.y, 
 		        &scene->lights[j].source.z); 

                /* light intensity */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->lights[j].intensity.r, &scene->lights[j].intensity.g,
		        &scene->lights[j].intensity.b); 

		fprintf (stderr, "\nlight %d source = %.1f, %.1f, %.1f\n", j,
                         scene->lights[j].source.x, scene->lights[j].source.y, scene->lights[j].source.z); 
  		fprintf (stderr, "light %d intensity = %.1f, %.1f, %.1f\n", j, 
       			 scene->lights[j].intensity.r, scene->lights[j].intensity.g, scene->lights[j].intensity.b);

      		++j;    
           }

           if (strcmp (line, "box") == 0)
           {
               scene->objs[i].intersect = intersect_box;
               scene->objs[i].color_ptr = get_color;

               /* box lower-left */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b1[0], 
                       &scene->objs[i].geom.box.b1[1], &scene->objs[i].geom.box.b1[2]);

               /* box upper-right */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b2[0], 
                       &scene->objs[i].geom.box.b2[1], &scene->objs[i].geom.box.b2[2]); 

                /* box color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

 		/* box reflectivity */
                skip_white_space (fp);
   		fscanf (fp, "%lf", &scene->objs[i].refl); 

 		fprintf (stderr, "\nbox %d ll = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b1[0], scene->objs[i].geom.box.b1[1], scene->objs[i].geom.box.b1[2]); 
                fprintf (stderr, "box %d ur = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b2[0], scene->objs[i].geom.box.b2[1], scene->objs[i].geom.box.b2[2]); 
                fprintf (stderr, "box %d color = %.1f, %.1f, %.1f\n", i, scene->objs[i].color.r,
                         scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "box %d refl = %.1f\n", i, scene->objs[i].refl);

 		++i; 
	    }
                
        }  
     
    }

    fclose (fp); 
}


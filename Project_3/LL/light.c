#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include "ray.h"
#include "vector.h"

/* Sphere lighting */
COLOR_T get_color (OBJ_T *obj, VEC_PT int_pt) 
{
    return obj->color;  
}

/* Plane lighting */
COLOR_T checkerboard (OBJ_T *obj, VEC_PT int_pt) 
{
    if ((((int) floor(int_pt.x)) + ((int) floor(int_pt.y)) + ((int) floor(int_pt.z))) & 1)
        return obj->color; 

    else
        return obj->color2; 
}

bool shadow_test (IMG_T image, OBJ_T *objs, RAY_T ray, VEC_PT int_pt, VEC_PT L, OBJ_T *closest_obj)
{
    VEC_PT norm; 
    double t; 
    OBJ_T *current_obj; 

    /* The intersection point on the plane becomes the new ray origin */
    ray.or = int_pt; 
    ray.dir = L;   

    for ( current_obj = objs; current_obj != NULL; current_obj = current_obj->next ) 
    {  
        if (current_obj != closest_obj) 
        {
            if (current_obj->intersect(current_obj, ray, &int_pt, &norm, &t) == true) 
                return true;
        } 
    }  

    return false; 
}

COLOR_T do_lighting (IMG_T image, SCENE_T scene, RAY_T ray, VEC_PT int_pt, VEC_PT norm, OBJ_T *closest_obj)
{
    const int n = 100;                 
    const double amb_factor = 0.10; 
    const double c1 = .002, c2 = .02, c3 = .2; 
    double atten, Dl;    
    VEC_PT L, R;             
    COLOR_T object_color, final_color;
    LIGHT_T *current_light; 

    /* Function pointer that calls either checkerboard (for planes)
       or get_color (for spheres) */
    object_color = closest_obj->color_ptr(closest_obj, int_pt);   

    /* Ambient Lighting */
    final_color.r = amb_factor * object_color.r; 
    final_color.g = amb_factor * object_color.g;
    final_color.b = amb_factor * object_color.b;
 
    for ( current_light = scene.lights; current_light != NULL; current_light = current_light->next )
    {
    	/* Calculate Vector L */
    	L = vec_subtract (current_light->source, int_pt); 

    	/* Light attenuation 
      	  |L| = magnitude of L = Dl */
    	Dl = vec_len (L); 	       
    	atten = 1 / (c1 * Dl * Dl + c2 * Dl + c3); 

    	L = vec_normalize (L);

    	if (shadow_test(image, scene.objs, ray, int_pt, L, closest_obj) == false)
    	{
            //current_obj = current_obj->next;  /* if curr_obj != NULL */

            if (vec_dot(norm, L) > 0) 
       	    {
	       	/* Diffuse lighting */
           	final_color.r += vec_dot(norm, L) * object_color.r * atten * 
                                 current_light->intensity.r; 
           	final_color.g += vec_dot(norm, L) * object_color.g * atten * 
				 current_light->intensity.g; 
           	final_color.b += vec_dot(norm, L) * object_color.b * atten * 
				 current_light->intensity.b; 
    
           	R.x = L.x - 2 * vec_dot(norm, L) * norm.x; 
           	R.y = L.y - 2 * vec_dot(norm, L) * norm.y;
           	R.z = L.z - 2 * vec_dot(norm, L) * norm.z;

           	R = vec_normalize (R);

           	if (vec_dot(R, ray.dir) > 0)     
           	{
               	    /* Specular lighting */
               	    final_color.r += pow (vec_dot(R, ray.dir), n) * atten * 
			             current_light->intensity.r; 
               	    final_color.g += pow (vec_dot(R, ray.dir), n) * atten *
				     current_light->intensity.g;  
               	    final_color.b += pow (vec_dot(R, ray.dir), n) * atten * 
				     current_light->intensity.b;  
                }
            }
        }
    }           

    return final_color;  
} 

COLOR_T refl_specular (IMG_T image, SCENE_T scene, RAY_T ray, VEC_PT int_pt, VEC_PT norm, COLOR_T refl_color)
{
    const int n = 100;                 
    const double c1 = .002, c2 = .02, c3 = .2; 
    double atten, Dl;    
    VEC_PT L, R;             
    COLOR_T final_color = refl_color;  

    LIGHT_T *current_light;
 
    for ( current_light = scene.lights; current_light != NULL; current_light = current_light->next )
    {
    	/* Calculate Vector L */
    	L = vec_subtract (current_light->source, int_pt); 

    	/* Light attenuation 
      	  |L| = magnitude of L = Dl */
    	Dl = vec_len (L); 	       
    	atten = 1 / (c1 * Dl * Dl + c2 * Dl + c3); 

    	L = vec_normalize (L);

        if (vec_dot(norm, L) > 0) 
       	{
            R.x = L.x - 2 * vec_dot(norm, L) * norm.x; 
            R.y = L.y - 2 * vec_dot(norm, L) * norm.y;
            R.z = L.z - 2 * vec_dot(norm, L) * norm.z;

            R = vec_normalize (R);

            if ( vec_dot(R, ray.dir) > 0 )     
            {
                /* Specular lighting */
               	final_color.r += pow (vec_dot(R, ray.dir), n) * atten * current_light->intensity.r; 
               	final_color.g += pow (vec_dot(R, ray.dir), n) * atten * current_light->intensity.g;  
               	final_color.b += pow (vec_dot(R, ray.dir), n) * atten * current_light->intensity.b;  
            }
        }
    }

    return final_color;  
} 


#include <stdio.h>
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "ray.h"
#include "box.h"
#include "sphere.h"
#include "plane.h"
#include "light.h"

typedef struct node {
  SCENE_T scene;
  struct node *next;
}  NODE_T; 

void printList(NODE_T* current)
{
	//first see if current == NULL, then print "empty list"
	
	//then keep printing the data and iterating through the nodes

        int i = 0;

        if (current != NULL) {

	   for ( i = 0; i < 4; ++i ) 
           {
              printf ("Entry no. %d = %f\n", i, current[i].scene.objs[i].refl);
              /*fprintf (stderr, "sphere %d center = {%.1f, %.1f, %.1f}\n", i, 
                       current->scene.objs[i].geom.sphere.center.x, current->scene.objs[i].geom.sphere.center.y, 
                       current->scene.objs[i].geom.sphere.center.z); */
             //++i;
           }
        }

        else
           printf ("Empty\n");  
}

OBJ_T* addToFront (OBJ_T *temp, OBJ_T data)
{
	//add a new node to the very front of the list
	//initialize the node, and point to the current head
	OBJ_T *new; 

        if (temp == NULL) {
            return; fprintf (stderr, "NULL\n"); 
        }

        if ((new = (OBJ_T *)malloc(sizeof(OBJ_T))) == NULL)
            exit (1); 

        *new = data;
        new->next = temp; 
	//returns new head
        return new; 
}

void skip_white_space (FILE *fp) 
{
    char ch; 

    ch = fgetc (fp); 

    while (isspace (ch))
        ch = fgetc (fp);       
 
    ungetc (ch, fp); 
}

void init (SCENE_T *scene, IMG_T *image, NODE_T *head)
{
    char line[100];
    char ch;
     
    int i = 0, j = 0; 

    FILE *fp;  

    if ((fp = fopen ("scene.txt", "r")) == NULL)
        fprintf (stderr, "Error: text file cannot be opened\n"); 

    while (fscanf (fp, "%s", line) != EOF)
    {
        if (strcmp (line, "sphere") == 0) ++image->num_obj;
        if (strcmp (line, "plane")  == 0) ++image->num_obj;
        if (strcmp (line, "box")    == 0) ++image->num_obj;
	if (strcmp (line, "light")  == 0) ++image->num_light;
    }

    LIGHT_T *new_light;
    OBJ_T *new_obj, *curr; 

    //new_light = (LIGHT_T *) malloc (sizeof(LIGHT_T));
    scene->objs = NULL; 	
    scene->lights = NULL; 
    
    rewind (fp);

    while (ch != EOF)
    {
        skip_white_space (fp); 
        ch = fgetc(fp); 

        if (isalpha(ch))
        {
            ungetc (ch, fp);
            fscanf (fp, "%s", line);  
            
            if (strcmp (line, "width") == 0) 
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->width);  
            }       

            if (strcmp (line, "height") == 0)
            {
                skip_white_space (fp); 
                fscanf (fp, "%d", &image->height);  
            } 

            if (strcmp (line, "sphere") == 0)
            {
		OBJ_T object;  

                object.intersect = intersect_sphere;
                object.color_ptr = get_color;

                /* sphere center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &object.geom.sphere.center.x, 
                        &object.geom.sphere.center.y, &object.geom.sphere.center.z);
     
                /* sphere radius */
    		skip_white_space (fp);
                fscanf (fp, "%lf", &object.geom.sphere.radius); 
 
                /* sphere color */
 		skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &object.color.r,
                        &object.color.g, &object.color.b);

                /* sphere reflectivity */
		skip_white_space (fp);
                fscanf (fp, "%lf", &object.refl);
                
                fprintf (stderr, "\nsphere %d center = {%.1f, %.1f, %.1f}\n", i, 
                         new_obj->geom.sphere.center.x, object.geom.sphere.center.y, object.geom.sphere.center.z); 
                fprintf (stderr, "sphere %d radius = %.1f\n", i, object.geom.sphere.radius); 
                fprintf (stderr, "sphere %d color = %.1f, %.1f, %.1f\n", i, object.color.r,
                         object.color.g, object.color.b);
                fprintf (stderr, "sphere %d refl = %.1f\n", i, object.refl);  
	
                //new_node[i].scene = *scene; 
                //new_node[i].next = curr; 
	        //curr = &new_node[i]; 
                scene->objs = addToFront (scene->objs, object);   

 
                ++i; 
            } 
	    
            if (strcmp (line, "plane") == 0)
            {
                OBJ_T object; 

                object.intersect = intersect_plane; 
                object.color_ptr = checkerboard;

                /* plane normal */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &object.geom.plane.norm.x, 
                        &object.geom.plane.norm.y, &object.geom.plane.norm.z);
 		#if 0
                /* plane D */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].geom.plane.D);

                /* plane color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

                /* plane color 2 */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color2.r, 
                        &scene->objs[i].color2.g, &scene->objs[i].color2.b);

                /* plane reflectivity */
                skip_white_space (fp); 
                fscanf (fp, "%lf", &scene->objs[i].refl); 
                
                fprintf (stderr, "\nplane %d norm = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].geom.plane.norm.x, 
                         scene->objs[i].geom.plane.norm.y, scene->objs[i].geom.plane.norm.z);
                fprintf (stderr, "plane %d color = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color.r, 
                        scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "plane %d color2 = {%.1f, %.1f, %.1f}\n", i, scene->objs[i].color2.r, 
                        scene->objs[i].color2.g, scene->objs[i].color2.b);
                fprintf (stderr, "plane %d reflectivity = %.1f\n", i, scene->objs[i].refl);
		#endif
		scene->objs = addToFront (scene->objs, object); 

                ++i;
            }
		#if 0
            if (strcmp (line, "light") == 0)
            { 
                //if ((new_node3 = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
		//    exit (1);

                /* light center */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->lights[j].source.x, &scene->lights[j].source.y, 
 		        &scene->lights[j].source.z); 

                /* light intensity */
                skip_white_space (fp); 
                fscanf (fp, "%lf %lf %lf", &scene->lights[j].intensity.r, &scene->lights[j].intensity.g,
		        &scene->lights[j].intensity.b); 

		fprintf (stderr, "\nlight %d source = %.1f, %.1f, %.1f\n", j,
                         scene->lights[j].source.x, scene->lights[j].source.y, scene->lights[j].source.z); 
  		fprintf (stderr, "light %d intensity = %.1f, %.1f, %.1f\n", j, 
       			 scene->lights[j].intensity.r, scene->lights[j].intensity.g, scene->lights[j].intensity.b);

		new_node[i].scene = *scene; 
                new_node[i].next = curr; 
	        curr = &new_node[i]; 

      		++i;    
           }
	   
           if (strcmp (line, "box") == 0)
           {
               //if ((new_node4 = (NODE_T *) malloc (sizeof(NODE_T))) == NULL)
	       //	    exit (1);

               scene->objs[i].intersect = intersect_box;
               scene->objs[i].color_ptr = get_color;

               /* box lower-left */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b1[0], 
                       &scene->objs[i].geom.box.b1[1], &scene->objs[i].geom.box.b1[2]);

               /* box upper-right */
 	       skip_white_space (fp); 

               fscanf (fp, "%lf %lf %lf", &scene->objs[i].geom.box.b2[0], 
                       &scene->objs[i].geom.box.b2[1], &scene->objs[i].geom.box.b2[2]); 

                /* box color */
                skip_white_space (fp);
                fscanf (fp, "%lf %lf %lf", &scene->objs[i].color.r, 
                        &scene->objs[i].color.g, &scene->objs[i].color.b);

 		/* box reflectivity */
                skip_white_space (fp);
   		fscanf (fp, "%lf", &scene->objs[i].refl); 

 		fprintf (stderr, "\nbox %d ll = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b1[0], scene->objs[i].geom.box.b1[1], scene->objs[i].geom.box.b1[2]); 
                fprintf (stderr, "box %d ur = {%.1f, %.1f, %.1f}\n", i, 
                         scene->objs[i].geom.box.b2[0], scene->objs[i].geom.box.b2[1], scene->objs[i].geom.box.b2[2]); 
                fprintf (stderr, "box %d color = %.1f, %.1f, %.1f\n", i, scene->objs[i].color.r,
                         scene->objs[i].color.g, scene->objs[i].color.b);
                fprintf (stderr, "box %d refl = %.1f\n", i, scene->objs[i].refl);

		new_node[i].scene = *scene; 
                new_node[i].next = curr; 
	        curr = &new_node[i]; 

 		++i;  
	    }
            #endif     
        }  
     	
    }
 
    image->num_obj = i;
    image->num_light = j;

    //scene->background = (COLOR_T) {0.2, 0, 0.3};

    fprintf (stderr, "\nnumber of objects = %d, number of lights = %d\n\n", i, j); 

    fclose (fp); 

    //return new_node;
}


int main (void)
{
    SCENE_T scene;
    IMG_T image;

    NODE_T *temp;

    init (&scene, &image, temp); 

    for ( ; scene.objs != NULL; scene.objs = scene.objs->next); 
        fprintf (stderr, "Hi\n"); 

    return 0;
}

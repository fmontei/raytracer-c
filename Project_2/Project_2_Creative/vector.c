#include "vector.h"
#include <math.h>

VEC_PT vec_normalize (VEC_PT vec)
{
    double length;
    VEC_PT result; 
    
    length = sqrt ( vec.x * vec.x + vec.y * vec.y + vec.z * vec.z); 
     
    result.x = vec.x / length;
    result.y = vec.y / length;
    result.z = vec.z / length;

    return result;  
}

double vec_dot (VEC_PT v1, VEC_PT v2)
{
    double result;

    result = (v1.x * v2.x) + 
             (v1.y * v2.y) + 
             (v1.z * v2.z); 

    return result;    
}  

VEC_PT vec_subtract (VEC_PT v1, VEC_PT v2)
{
    VEC_PT result;

    result.x = v1.x - v2.x; 
    result.y = v1.y - v2.y; 
    result.z = v1.z - v2.z;
 
    return result;
}

double vec_len (VEC_PT v) 
{
    double result;

    result = sqrt (v.x * v.x + v.y * v.y + v.z * v.z);
    
    return result; 
}

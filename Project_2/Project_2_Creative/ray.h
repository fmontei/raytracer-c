#define HEIGHT  768
#define WIDTH  1024
#define MAXV    255 
#define OBJ_NUM  37
#define LIGHT_NUM 4

#ifndef _RAY_H
#define _RAY_H

#include <stdbool.h>
#include "vector.h"

typedef struct {
  double r;
  double g;
  double b; 
}  COLOR_T; 

typedef struct {
  VEC_PT or;
  VEC_PT dir;
}  RAY_T;

typedef struct {
  VEC_PT source;
  COLOR_T intensity;
}  LIGHT_T; 

typedef struct { 
  VEC_PT center;
  double radius;
}  SPHERE_T; 

typedef struct {
  VEC_PT norm;
  double D;
}  PLANE_T; 

typedef struct OBJ_TYPE {
  int type; 
  COLOR_T color, color2; 
  bool (*intersect) (struct OBJ_TYPE, RAY_T, VEC_PT *, VEC_PT *, double *); 
  COLOR_T (*color_ptr) (struct OBJ_TYPE, VEC_PT); 
  
   union {
    SPHERE_T sphere; 
    PLANE_T plane;
  }  geom;
   
}  OBJ_T; 

typedef struct {
  LIGHT_T lights[LIGHT_NUM];
  OBJ_T objs[OBJ_NUM];
  COLOR_T background;  
}  SCENE_T;
  
#endif
